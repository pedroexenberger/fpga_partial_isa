#! /usr/bin/python

#README script to open a stats.txt file, output from gem5 simulation and extract siginificative parameters from it


import sys
import os.path
import re
import metrics

def isPercent(str):
    if re.match(r'\d+\.\d+%', str) is None:
        return False
    else:
        return True

####### Script starts here

benchmarks = ["basicmath","bitcount","blowfish-e", "cnn",
     "CRC32","dijkstra","FFT","FFT-i","gsm-d","gsm-e", "h264",
     "jpeg-d","jpeg-e","patricia","qsort","rsynth","sha","stringsearch",
     "susan-c","susan-e","susan-s", "correlation", "gemm", "gesummv", "2mm", "3mm",
        "cholesky", "durbin", "gramschmidt", "ludcmp", "floyd-warshall",
        "jacobi-1d"]
#benchmarks = ["FFT"]
sizes = ["small"]
processors = ["A15"] #usar o a15 aqui porque ja esta de acordo com o print esperado no metrics.py
isa = "RISCV"

target_folder="gem5/m5out"

extracted_stats_filename = "summarized_stats.txt"
extracted_stats_filename_short = "summarized_stats_short.txt"

fp_extracted_stats = open(extracted_stats_filename,"w")
fp_extracted_stats_short = open(extracted_stats_filename_short,"w")

#Make header
fp_extracted_stats.write('{:<15s}'.format("benchmark"));
for param_key in metrics.ordered_parameters_map:
    fp_extracted_stats.write(' {:<12s}'.format(metrics.ordered_parameters_map[param_key]))
fp_extracted_stats.write("\n");

#Make header short
fp_extracted_stats_short.write('{:<15s}'.format("benchmark"));
fp_extracted_stats_short.write(' {:<12s}'.format("FP"));
fp_extracted_stats_short.write(' {:<12s}'.format("Other"));
fp_extracted_stats_short.write("\n");

for processor in processors:
    for size in sizes:
        for benchmark in benchmarks:
            fp_accumulator = 0.0;
            stats_filename = "{0}_{1}_{2}sw.txt".format(benchmark, size, processor)

            print ("Extracting data from: {0}".format(stats_filename))

            stats_file = target_folder + "/" + stats_filename
                
            fp_extracted_stats.write('{:<15s}'.format(benchmark));
            fp_extracted_stats_short.write('{:<15s}'.format(benchmark));
            with open(stats_file,"r") as fp_stats_file:
                for line in fp_stats_file:
                    for param_key in metrics.ordered_parameters_map:
                        if not param_key in line:
                            continue
                        print(param_key)
                        stats_line = line.split()
                        param_value = stats_line[1]
                        if isPercent(stats_line[2]): #if available percentually, change absolute for % value
                            param_value = stats_line[2]
                        fp_extracted_stats.write( ' {:<12s}'.format(param_value) )
                        if ("Float" in param_key):
                            print("{0} valor {1} formatado {2}".format(param_key, param_value, str(float(param_value.replace('%','')))))
                            fp_accumulator += float(param_value.replace('%',''))
            fp_extracted_stats.write("\n")
            fp_extracted_stats_short.write( ' {:<12s}'.format(str(fp_accumulator)) )
            fp_extracted_stats_short.write( ' {:<12s}'.format(str(100- fp_accumulator)) )
            fp_extracted_stats_short.write("\n")

fp_extracted_stats.close()
fp_extracted_stats_short.close()
            
