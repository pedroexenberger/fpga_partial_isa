import os
import subprocess

## Helper function
## Merges two dictionaries x and y, returning a new one with x's and y's keys
## (when appliable, y's keys replace x's keys)
def merge_two_dicts(x, y):
    z = x.copy()
    z.update(y)
    return z

## Don't judge me.
def merge_six_dicts(a, b, c, d, e, f):
    z = a.copy()
    z.update(b)
    z.update(c)
    z.update(d)
    z.update(e)
    z.update(f)
    return z

class MiBenchBenchmarkManager:
    base_dir = "/home/pedro/Projects/fpga_partial_isa/mibench"
    out_dir  = ""

    automotive = [
        "basicmath",
        "bitcount",
        "qsort",
        "susan-c",
        "susan-e",
        "susan-s"]
    consumer = [
        "jpeg-e",
        "jpeg-d",
        "lame"]
    network = [
        "dijkstra",
        "patricia"]
    office = [
        "stringsearch",
        "rsynth"]
    security = [
        "blowfish-e",
        "blowfish-d",
        "rijndael",
        "sha"]
    telecomm = [
        #        "adpcm-e", # These two will never work, since their default
                            # behaviour is to read from stdin (which requires
                            # sending options directly to gem5, bypassing
                            # the python scripts) and I'm too lazy to fix it.
        #        "adpcm-d"
        "CRC32",
        "FFT",
        "FFT-i",
        "gsm-e",
        "gsm-d"]
    benchmarks = automotive + consumer + network + office + security + telecomm

    input_sets = ["small", "large"]
    automotive_in = {
        "basicmath"    : "<NA>",
        "bitcount"     : "<NA>",
        "qsort"        : "input_<INPUT_SET>.dat",
        "susan-c"      : "input_<INPUT_SET>.pgm",
        "susan-e"      : "input_<INPUT_SET>.pgm",
        "susan-s"      : "input_<INPUT_SET>.pgm"}
    consumer_in = {
        "jpeg-e"       : "input_<INPUT_SET>.ppm",
        "jpeg-d"       : "input_<INPUT_SET>.jpg",
        "lame"         : "<INPUT_SET>.wav"}
    network_in = {
        "dijkstra"     : "input.dat",
        "patricia"     : "<INPUT_SET>.udp"}
    office_in = {
        "stringsearch" : "<NA>",
        "rsynth"       : "<INPUT_SET>input.txt"}
    security_in = {
        "blowfish-e"   : "input_<INPUT_SET>.asc",
        "blowfish-d"   : "output_<INPUT_SET>.asc",
        # "rijndael"     : "input_<INPUT_SET>.asc",
        "rijndael"     : "output_<INPUT_SET>.enc",
        "sha"          : "input_<INPUT_SET>.asc"}
    telecomm_in = {
#        "adpcm-e"      : "data/<INPUT_SET>.pcm",
#        "adpcm-d"      : "data/<INPUT_SET>.adpcm",
        "CRC32"        : "<INPUT_SET>.pcm",
        "FFT"          : "<NA>",
        "FFT-i"        : "<NA>",
        "gsm-e"        : "data/<INPUT_SET>.au",
        "gsm-d"        : "data/<INPUT_SET>.au.run.gsm"}
    benchmarks_inputs = merge_six_dicts(
        automotive_in, consumer_in, network_in,
        office_in, security_in, telecomm_in)

    automotive_bin = {
        "basicmath"    : "basicmath_<INPUT_SET>",
        "bitcount"     : "bitcnts",
        "qsort"        : "qsort_<INPUT_SET>",
        "susan-c"      : "susan",
        "susan-e"      : "susan",
        "susan-s"      : "susan"}
    consumer_bin = {
        "jpeg-e"       : "jpeg-6a/cjpeg",
        "jpeg-d"       : "jpeg-6a/djpeg",
        "lame"         : "lame3.70/lame"} 
    network_bin = {
        "dijkstra"     : "dijkstra_<INPUT_SET>",
        "patricia"     : "patricia"}
    office_bin = {
        "stringsearch" : "search_<INPUT_SET>",
        "rsynth"       : "say"}
    security_bin = {
        "blowfish-e"   : "bf",
        "blowfish-d"   : "bf",
        "rijndael"     : "rijndael",
        "sha"          : "sha"}
    telecomm_bin = {
#        "adpcm-e"      : "bin/rawcaudio",
#        "adpcm-d"      : "bin/rawdaudio",
        "CRC32"        : "crc",
        "FFT"          : "fft",
        "FFT-i"        : "fft",
        "gsm-e"        : "bin/toast",
        "gsm-d"        : "bin/untoast"}
    benchmarks_bin = merge_six_dicts(
        automotive_bin, consumer_bin, network_bin,
        office_bin, security_bin, telecomm_bin)

    automotive_opts_small = {
        "basicmath"    : "<NA>",
        "bitcount"     : "75000",
        "qsort"        : "<DIR_IN>input_small.dat",

        "susan-c"      :
            "<DIR_IN>input_small.pgm <DIR_OUT>output_small.corners.pgm -c",
        "susan-e"      :
            "<DIR_IN>input_small.pgm <DIR_OUT>output_small.edges.pgm -e",
        "susan-s"      :
            "<DIR_IN>input_small.pgm <DIR_OUT>output_small.smoothing.pgm -s"}
    consumer_opts_small = {
        "jpeg-e"       :
            "-dct int -progressive -opt " + \
            "-outfile <DIR_OUT>output_small_encode.jpeg " + \
            "<DIR_IN>input_small.ppm",
        "jpeg-d"       : "-dct int -ppm " + \
            "-outfile <DIR_OUT>/output_small_decode.ppm " + \
            "<DIR_IN>input_small.jpg",
        "lame"         : "<DIR_OUT>/output_small.mp3"}
    network_opts_small = {
        "dijkstra"     : "<DIR_IN>input.dat",
        "patricia"     : "<DIR_IN>small.udp"}
    office_opts_small = {
        "stringsearch" : "<NA>",
        "rsynth"       : "-o small.au < smallinput.txt"}
    security_opts_small = {
        "blowfish-e"   :
            "e <DIR_IN>input_small.asc <DIR_OUT>output_small.enc "  + \
            "1234567890abcdeffedcba0987654321",
        "blowfish-d"   :
            "d <DIR_IN>output_small.enc <DIR_OUT>output_small.asc " + \
            "1234567890abcdeffedcba0987654321",
        # "rijndael"     :
        #     "<DIR_IN>input_small.asc <DIR_OUT>output_small.enc e " + \
        #     "1234567890abcdeffedcba09876543211234567890abcdeffedcba0987654321",
        "rijndael"     :
            "<DIR_IN>output_small.enc <DIR_OUT>output_small.dec d " + \
            "1234567890abcdeffedcba09876543211234567890abcdeffedcba0987654321",
        "sha"          :
            "<DIR_IN>input_small.asc"}
    telecomm_opts_small = {
#        "adpcm-e"      : "",
#        "adpcm-d"      : "",
        "CRC32"        : "<DIR_IN>small.pcm",
        "FFT"          : "4 4096",
        "FFT-i"        : "4 8192 -i",
        "gsm-e"        : "-fps -c <DIR_IN>small.au",
        "gsm-d"        : "-fps -c -u <DIR_IN>small.au.run.gsm"}
    benchmarks_opts_small = merge_six_dicts(
        automotive_opts_small, consumer_opts_small,
        network_opts_small, office_opts_small,
        security_opts_small, telecomm_opts_small)

    def __init__(self,
                 base_dir,
                 out_dir):
        self.base_dir = base_dir
        self.out_dir  = out_dir + "/"

        # Set up benchmarks directory
        self.benchmarks_dir = {}
        for b in self.automotive:
            self.benchmarks_dir[b] = self.base_dir   + \
                                     "/automotive/"  + \
                                     b.split("-")[0] + "/"
        for b in self.consumer:
            self.benchmarks_dir[b] = self.base_dir   + \
                                     "/consumer/"    + \
                                     b.split("-")[0] + "/"
        for b in self.network:
            self.benchmarks_dir[b] = self.base_dir   + \
                                     "/network/"     + \
                                     b.split("-")[0] + "/"
        for b in self.office:
            self.benchmarks_dir[b] = self.base_dir   + \
                                     "/office/"      + \
                                     b.split("-")[0] + "/"
        for b in self.security:
            self.benchmarks_dir[b] = self.base_dir   + \
                                     "/security/"    + \
                                     b.split("-")[0] + "/"
        for b in self.telecomm:
            self.benchmarks_dir[b] = self.base_dir   + \
                                     "/telecomm/"    + \
                                     b.split("-")[0] + "/"

    @staticmethod
    def isValidBenchmark(name):
        if name in self.benchmarks:
            return True
        else:
            return False

    def prepareRun(self, bench, input_set):
        out_dir   = self.out_dir + "run/"

        # Cleanup, just in case
        subprocess.call(["rm", "-rf", out_dir])
        subprocess.call(["mkdir", "-p", out_dir])

        # Copies binary to the out dir
        subprocess.call(
            ["cp",
             self.benchmarks_dir[bench] + \
                 self.benchmarks_bin[bench].replace("<INPUT_SET>", input_set),
             out_dir + bench + ".bin"])

        # Copies input to the out dir (if necessary)
        if self.benchmarks_inputs[bench] != "<NA>":
            print self.benchmarks_dir[bench] + \
                 self.benchmarks_inputs[bench].\
                 replace("<INPUT_SET>", input_set)
            subprocess.call(
            ["cp",
             self.benchmarks_dir[bench] + \
                 self.benchmarks_inputs[bench].
                 replace("<INPUT_SET>", input_set),
             out_dir])



    def getCmd(self, bench, input_set):
        out_dir = self.out_dir + "run/"
        return out_dir + bench + ".bin"

    def getOpts(self, bench, input_set):
        out_dir  = self.out_dir + "run/"

        if input_set == "small":
            return self.benchmarks_opts_small[bench].\
                replace("<DIR_IN>", out_dir).\
                replace("<DIR_OUT>", out_dir)
        elif input_set == "large":
            raise Exception("FATAL: large input set not implemented.")
        else:
            raise Exception("FATAL")

class PARSECBenchmarkManager:
    parsec_dir = "/data/tese/Tools/Benchmarks/parsec-3.0-lite"

    parsec_cfgs = {
        # Single-threaded configurations
        "x86-st"   : "amd64-linux.gcc-serial",
        "riscv-st" : "amd64-linux.riscv-serial"}
    default_parsec_cfg = 'x86-st'
    parsec_cfg = "" # Set in the initializer

    out_dir    = ""

    apps = [
        "blackscholes",
        "bodytrack",
        "ferret",
        "fluidanimate",
#        "raytrace", fails with fatal: syscall prlimit64 (#302) unimplemented
        "swaptions"]
    kernels = [
        "canneal",
        "streamcluster"]

    benchmarks = apps + kernels
    input_sets = ["test", "simsmall", "simmedium", "simlarge"]

    @staticmethod
    def testValidCombi(bench, input_set):
        if bench not in PARSECBenchmarkManager.benchmarks:
            raise ValueError(bench + " not in " + benchmarks + ".")
        if input_set not in PARSECBenchmarkManager.input_sets:
            raise ValueError(input_set + " not in " + input_sets + ".")

    def __init__(self,
                 parsec_dir,
                 parsec_cfg,
                 out_dir):
        self.parsec_dir = parsec_dir
        if parsec_cfg not in self.parsec_cfgs:
            raise ValueError(parsec_cfg + " not in " +
                             str(self.parsec_cfgs.keys()) + ".")
        self.parsec_cfg = self.parsec_cfgs[parsec_cfg]
        self.out_dir    = out_dir + "/"

        self.benchmarks_dir = {}
        for b in self.apps:
            self.benchmarks_dir[b] = self.parsec_dir  + \
                                     "/pkgs/apps/"    + \
                                     b + "/"
        for b in self.kernels:
            self.benchmarks_dir[b] = self.parsec_dir  + \
                                     "/pkgs/kernels/" + \
                                     b + "/"

    def isPARSEC(self, name):
        if name in self.benchmarks:
            return True
        else:
            return False

    def prepareRun(self, bench, input_set):
        """Extracts the gzip'ed input_set located in the benchmark's
           input dir and stores in the output dir. Likewise, also copy
           the benchmark's binary."""

        PARSECBenchmarkManager.testValidCombi(bench, input_set)

        inst_dir  = self.benchmarks_dir[bench] + "inst/" + \
                    self.parsec_cfg + "/"
        input_dir = self.benchmarks_dir[bench] + "inputs/"
        out_dir   = self.out_dir + "run/"

        # Cleanup, just in case
        subprocess.call(["rm", "-rf", out_dir])
        subprocess.call(["mkdir", "-p", out_dir])


        # Extracts input set, except for swaptions and streamcluster
        # (no input set).
        if bench != "swaptions" and bench != "streamcluster":
            filepaths = [
                # Unzipped input
                input_dir + "input_" + input_set + ".tar",
                # Zipped input
                input_dir + input_set + ".tar.gz"]


            if os.path.isfile(filepaths[0]):
                subprocess.call(["tar",
                                 "-xf", filepaths[0],
                                 "-C",   out_dir])
            elif os.path.isfile(filepaths[1]):
                subprocess.call(["tar",
                                 "-xzf", filepaths[1],
                                 "-C",   out_dir])
            else:
                raise ValueError("Input file not found")


        # Copies binary (yes, binary name depends on the benchmark - it sucks).
        # Luckily, there's only one exception to the pattern.
        if bench != "raytrace":
            subprocess.call(["cp",
                             inst_dir + "/bin/" + bench,
                             out_dir + bench + ".bin"])
        else:
            subprocess.call(["cp",
                             inst_dir + "/bin/" + "rtview",
                             out_dir + bench + ".bin"])
        subprocess.call(["cp",
                         inst_dir + "build-info",
                         out_dir  + "build-info"])

    def getCmd(self, bench, input_set):

        PARSECBenchmarkManager.testValidCombi(bench, input_set)

        out_dir   = self.out_dir + "run/"
        return out_dir + bench + ".bin"

    def getOpts(self, bench, input_set):

        PARSECBenchmarkManager.testValidCombi(bench, input_set)

        outdir  = self.out_dir + "run/"
        nthread = " " + str(1) + " "

        test = {
            "blackscholes" :
            nthread + outdir + "in_4.txt " + outdir + "prices.txt",

            "bodytrack" :
            outdir + "sequenceB_1" + " 4 1 5 1 0 " + nthread,

            "fluidanimate" :
            nthread + "1 " + outdir + "in_5K.fluid " + outdir + "out.fluid",

            "raytrace" :
            outdir + "octahedron.obj " + "-automove -nthreads" + \
            nthread + "-frames 1 -res 1 1",

            "swaptions" :
            "-ns 1 -sm 5 -nt " + nthread,

            "canneal" :
            nthread + "5 100 " + outdir + "10.nets " + "1",

            "streamcluster" :
            "2 5 1 10 10 5 none " + outdir + "output.txt" + nthread
        }

        simsmall = {
            "blackscholes" :
            nthread + outdir + "in_4K.txt " + outdir + "prices.txt",

            "bodytrack" :
            outdir + "sequenceB_1" + " 4 1 1000 5 0 " + nthread,

            "fluidanimate" :
            nthread + "1 " + outdir + "in_35K.fluid " + outdir + "out.fluid",

            "raytrace" :
            outdir + "happy_buddha.obj " + "-automove -nthreads" + \
            nthread + "-frames 3 -res 480 270",

            "swaptions" :
            "-ns 16 -sm 10000 -nt " + nthread,

            "canneal" :
            nthread + "10000 2000 " + outdir + "100000.nets " + "32",

            "streamcluster" :
            "10 20 32 4096 4096 1000 none " + outdir + "output.txt" + nthread
        }

        simmedium = {
            "blackscholes" :
            nthread + outdir + "in_16K.txt " + outdir + "prices.txt",

            "bodytrack" :
            outdir + "sequenceB_2" + " 4 2 2000 5 0 " + nthread,

            "fluidanimate" :
            nthread + "1 " + outdir + "in_100K.fluid " + outdir + "out.fluid",

            "raytrace" :
            outdir + "happy_buddha.obj " + "-automove -nthreads" + \
            nthread + "-frames 3 -res 960 540",

            "swaptions" :
            "-ns 32 -sm 20000 -nt " + nthread,

            "canneal" :
            nthread + "15000 2000 " + outdir + "200000.nets " + "64",

            "streamcluster" :
            "10 20 64 8192 8192 1000 none " + outdir + "output.txt" + nthread
        }


        if input_set == "test":
            return test[bench]
        elif input_set == "simsmall":
            return simsmall[bench]
        elif input_set == "simmedium":
            return simmedium[bench]
        elif input_set == "simlarge":
            raise Exception("FATAL: large input set not implemented.")
        else:
            raise Exception("FATAL")

        return opts[name]

if __name__ == "__main__":
    import sys

    benchmark = sys.argv[1]

    mgr = PARSECBenchmarkManager()

    print(benchmark)
    print(mgr.getCmd(benchmark) + " " + mgr.getOpts(benchmark))
