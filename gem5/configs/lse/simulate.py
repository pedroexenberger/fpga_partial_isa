# -*- coding: utf-8 -*-

from __future__     import print_function

import m5
from m5.objects     import *
from m5.util        import addToPath

from argparse       import ArgumentParser

addToPath("./systems/")
addToPath("./systems/cpus/")
addToPath("./systems/caches/")

from benchmarks     import PARSECBenchmarkManager
from benchmarks     import MiBenchBenchmarkManager

from system_manager import SystemManager

# Beautiful COLors for formatting
class bcol:
    MAGENTA    = '\033[95m'
    BLUE       = '\033[94m'
    GREEN      = '\033[92m'
    YELLOW     = '\033[93m'
    RED        = '\033[91m'
    BOLD       = '\033[1m'
    UNDERLINE  = '\033[4m'
    END        = '\033[0m'

    @staticmethod
    def H(msg):
        return bcol.MAGENTA + msg + bcol.END

    @staticmethod
    def I(msg):
        return bcol.BLUE + msg + bcol.END

    @staticmethod
    def W(msg):
        return bcol.YELLOW + msg + bcol.END

    @staticmethod
    def OK(msg):
        return bcol.GREEN + msg + bcol.END

    @staticmethod
    def E(msg):
        return bcol.RED + msg + bcol.END


print(bcol.H( \
"***************************************************************************"))
print(bcol.H( \
"********************** gem5 configuration script **************************"))
print(bcol.H( \
"*************************** lse/simulate.py *******************************"))
print(bcol.H( \
"***************************************************************************"))
print("")

############################################################
## Script options
############################################################

main_parser = ArgumentParser(description = "Top-level simulation script.")
main_parser.add_argument("--env", default = "",
                    help = """List of ";-separated" environment variables to be
                    set before running the binary. Ex: --env "VAR1=X;VAR2=Y""")
main_parser.add_argument("--cpu", default = "Generic-OoO-1-Issue",
                    help = "CPU to use.",
                    choices = SystemManager.available_cpus.keys())
main_parser.add_argument("--num-cpus", default = 1,
                    help = "Number of CPUs in a multicore processor.")

subparsers = main_parser.add_subparsers(
    title = "Commands available",
    help  = "Use \"<command> --help\" for additional options.",
    dest = "subcommand")
parser_parsec = subparsers.add_parser(
    'run-parsec-3.0',
    help = 'Runs a benchmark from PARSEC-3.0.')
parser_mibench = subparsers.add_parser(
    'run-mibench',
    help = 'Runs a benchmark from MiBench.')
parser_other  = subparsers.add_parser(
    'run-other',
    help = "Runs your own benchmark.")

# PARSEC benchmark options
parser_parsec.add_argument(
    "--parsec-dir",
    help = "Root PARSEC-3.0 directory. " + \
    "Default: \"" + PARSECBenchmarkManager.parsec_dir + "\"",
    default = PARSECBenchmarkManager.parsec_dir)
parser_parsec.add_argument(
    "--benchmark",
    help = "Benchmark to use. Default: \"blackscholes\".",
    default = "blackscholes",
    choices = PARSECBenchmarkManager.benchmarks)
parser_parsec.add_argument(
    "--parsec-cfg",

    help = "Configuration to use. Default: \"" + \
    PARSECBenchmarkManager.default_parsec_cfg + "\".",

    default = PARSECBenchmarkManager.default_parsec_cfg)
parser_parsec.add_argument(
    "--input-set",
    help = "Input set to use. Default: \"test\".",
    default = "test",
    choices = PARSECBenchmarkManager.input_sets)

# MiBench benchmark options
parser_mibench.add_argument(
    "--mibench-dir",
    help = "Root MiBench directory. " + \
    "Default : \"" + MiBenchBenchmarkManager.base_dir + "\"",
    default = MiBenchBenchmarkManager.base_dir)
parser_mibench.add_argument(
    "--benchmark",
    help = "Benchmark to use. Default: \"basicmath\".",
    default = "basicmath",
    choices = MiBenchBenchmarkManager.benchmarks)
parser_mibench.add_argument(
    "--input-set",
    help = "Input set to use. Default: \"small\".",
    default = "small",
    choices = MiBenchBenchmarkManager.input_sets)

# Other benchmarks
parser_other.add_argument(
    "-c", "--cmd", default = "",
    help = "Binary file to run.")
parser_other.add_argument(
    "-o", "--options", default = "",
    help = """Command-line options to the binary.
    Use "" around the string.""")

args = main_parser.parse_args()

############################################################
# Setup system
############################################################

print(bcol.I("Setting up system ..."))
print(bcol.W("Using " + args.cpu + " CPU."))
system = SystemManager().getSystem(args.cpu, args.num_cpus)
print(bcol.OK("Done."))
print("")

############################################################
# Setup process
############################################################

print(bcol.I("Setting up the process to run ..."))
process = Process()
if args.subcommand == "run-parsec-3.0":

    benchMgr = PARSECBenchmarkManager(
        args.parsec_dir,
        args.parsec_cfg,
        m5.options.outdir)

    print(bcol.W("PARSEC Benchmark " + args.benchmark + \
                         " selected."))
    print(bcol.W("\"" + args.input_set + "\"" + \
                 " input set selected."))
    print(bcol.I("Setting up benchmark output directory ..."))
    benchMgr.prepareRun(args.benchmark, args.input_set)

    process.cmd  = benchMgr.getCmd(args.benchmark, args.input_set)
    process.cmd += benchMgr.getOpts(args.benchmark, args.input_set).split()

    print(bcol.OK("Done"))
elif args.subcommand == "run-mibench":

    benchMgr = MiBenchBenchmarkManager(
        args.mibench_dir,
        m5.options.outdir)

    print(bcol.W("MiBench Benchmark " + args.benchmark + \
                         " selected."))
    print(bcol.I("Setting up benchmark output directory ..."))
    benchMgr.prepareRun(args.benchmark, args.input_set)

    process.cmd  = benchMgr.getCmd(args.benchmark, args.input_set)
    process.cmd += benchMgr.getOpts(args.benchmark, args.input_set).split()

    print(bcol.OK("Done"))

elif args.subcommand == "run-other":
    print(bcol.W("Custom benchmark selected."))
    print(bcol.W("Command           : " + args.cmd))
    print(bcol.W("Command-line args : " + (args.options
                                          if args.options else "(none)")))

    if args.cmd:
        process.executable = args.cmd
        process.cmd = args.cmd

        if args.options != "": # Args.args is a string
            process.cmd += args.options.split() # That becomes a list here
    else:
        exit(bcol.E("Must specify the program to run (--cmd <program>)."))
    print(bcol.OK("Done"))
else:
    exit(bcol.E("FATAL: command ") + bcol.E(args.subcommand) + \
         bcol.E(" is invalid."))
print("")

############################################################
## Setup environmental variables
############################################################

print(bcol.I("Setting environmental variables ..."))
if args.env:
    evars = args.env.split(";")
    process.env = evars

    print(bcol.W("\n".join([str(i) for i in evars])))
else:
    print(bcol.W("(no environmental variables to set)"))
print(bcol.OK("Done"))
print("")

############################################################
## Instantiate stuff
############################################################

print(bcol.I("Instantiating process ..."))
print(bcol.I("Program to run: " + \
                "".join([str(i)+" " for i in process.cmd])))

for core in system.cpu:
    core.workload = process
    core.createThreads()
print(bcol.OK("Done"))
print("")

print(bcol.I("Instantiating system ..."))
root = Root(full_system = False, system = system)
m5.instantiate()
print(bcol.OK("Done"))
print("")

print(bcol.H( \
"*********************** Beginning gem5 simulation ************************"))
simulation = m5.simulate()
if simulation.getCause() == "exiting with last active thread context":
    print(bcol.H( \
"**************************************************************************"))
    print(bcol.OK("Done."))
    print(bcol.OK("Finished at tick %i." %(m5.curTick())))
    print(bcol.OK("Status: %s." % (simulation.getCause())))
    print(bcol.H( \
"**************************************************************************"))
else:
    print(bcol.H( \
"**************************************************************************"))
    print(bcol.E("Status: %s." % (simulation.getCause())))
    print(bcol.H( \
"**************************************************************************"))
