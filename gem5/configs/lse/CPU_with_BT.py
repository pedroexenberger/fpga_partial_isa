# -*- coding: utf-8 -*-

import m5
from m5.objects import *

from m5.objects import BaseCache
from m5.objects import DDR3_1600_8x8
from m5.objects import System
from m5.objects import SystemXBar
from m5.objects import SystemXBar

class L1Cache(Cache):

    size = '32kB'
    assoc = 4
    tag_latency = 2
    data_latency = 2
    response_latency = 2
    mshrs = 4
    tgts_per_mshr = 20

    def __init__(self, options=None):
        super(L1Cache, self).__init__()
        pass

    def connectToICache(self, cpu):
        self.cpu_side = cpu.icache_port

    def connectToDCache(self, cpu):
        self.cpu_side = cpu.dcache_port

    def connectBus(self, bus):
        self.mem_side = bus.slave

############################################################
############################################################
##
## Sistema
##
############################################################
############################################################
class BT_System(System):

    def __init__(self, options=None):
        super(BT_System, self).__init__()

############################################################
## clk_domain - Mexer somente na frequencia
############################################################
        self.clk_domain = SrcClockDomain()
        self.clk_domain.voltage_domain = VoltageDomain()
        self.clk_domain.clock = '2GHz'

############################################################
## mem_mode e mem_ranges - Não mexer
############################################################
        self.mem_mode = 'atomic'
        self.mem_ranges = [AddrRange('512MB')]

############################################################
## Cpu - definida em cpu.py
############################################################
        self.cpu    = AtomicSimpleCPU()
        self.BTUnit = BTUnit()

        self.BTUnit.num_rows      = 30
        self.BTUnit.cycle_size    =  3
        self.BTUnit.num_branches  =  2
        self.BTUnit.num_insts     =  0

        IntALU = FUDesc()
        IntALU.count  = 4
        IntALU.opList = [
            OpDesc(opClass='IntAdd'),
            OpDesc(opClass='IntSub'),
            OpDesc(opClass='IntLogic'),
            OpDesc(opClass='IntShift'),
            OpDesc(opClass='IntMov'),
            OpDesc(opClass='IntCtrl'),
            OpDesc(opClass='IntALU')]

        MulALU = FUDesc()
        MulALU.count = 2
        MulALU.opList = [
            OpDesc(opClass='IntMult', opLat=9)
        ]

        ReadPort = FUDesc()
        ReadPort.opList = [ OpDesc(opClass='MemRead', opLat=6 ) ]
        ReadPort.count = 2

        WritePort = FUDesc()
        WritePort.opList = [ OpDesc(opClass='MemWrite') ]
        WritePort.count = 1

        self.BTUnit.supported_ops = [ IntALU, MulALU, ReadPort, WritePort ]

        self.BTUnit.connectCPU (self.cpu)

############################################################
## Caches - definidas em caches.py
############################################################
        self.cpu.icache  = L1Cache()
        self.cpu.dcache  = L1Cache()


############################################################
## Define a interconexão entre as caches e memória principal
## Modifique aqui para alterar a hierarquia de memória.
############################################################
        self.membus = SystemXBar()

        self.cpu.icache.connectToICache(self.cpu)
        self.cpu.icache.connectBus(self.membus)
        self.cpu.dcache.connectToDCache(self.cpu)
        self.cpu.dcache.connectBus(self.membus)

############################################################
## Controlador de interrupções - conectado à memória principal
## Não mexer.
############################################################
        self.cpu.createInterruptController()
        self.cpu.interrupts[0].pio = self.membus.master
        self.cpu.interrupts[0].int_master = self.membus.slave
        self.cpu.interrupts[0].int_slave = self.membus.master

        self.system_port = self.membus.slave

############################################################
## Controlador de memória - conectado à memória principal
## Não mexer?
############################################################
        self.mem_ctrl = DDR3_1600_8x8()
        self.mem_ctrl.range = self.mem_ranges[0]
        self.mem_ctrl.port = self.membus.master
