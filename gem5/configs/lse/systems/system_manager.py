from m5.util      import addToPath

# CPUs
from ooo_1_issue  import GenericOoO1IssueCPU
from ooo_2_issue  import GenericOoO2IssueCPU
from ooo_3_issue  import GenericOoO3IssueCPU
from ooo_4_issue  import GenericOoO4IssueCPU
from ooo_5_issue  import GenericOoO5IssueCPU
from ooo_6_issue  import GenericOoO6IssueCPU
from ex5_LITTLE   import ex5_LITTLE
from ex5_big   import ex5_big



from m5.objects   import AtomicSimpleCPU

# System
from base_system  import BaseSystem

# Caches
from basic_caches import BasicL1Cache
from basic_caches import BasicL2Cache
from basic_caches import BasicL3Cache

# DCA - Dynamic Code Acceleration
from m5.objects   import BTUnit
from m5.objects   import CfgCache

# Memory bus
from m5.objects import SystemXBar

class MyCfgCache(CfgCache):
    size = 128

class SystemManager:

    available_cpus = {
        "Generic-OoO-1-Issue" :         0,
        "Generic-OoO-2-Issue" :         1,
        "Generic-OoO-3-Issue" :         2,
        "Generic-OoO-4-Issue" :         3,
        "Generic-OoO-5-Issue" :         4,
        "Generic-OoO-6-Issue" :         5,
        "Generic-OoO-4-Issue-with-BT" : 6,
        "A7" :                          7,
        "A15" :                         8}

    def getCPU(self, name):

        # Tests if other processors
        if name not in self.available_cpus:
            #for some reason, defaulting an exit doesnt work
            exit("Unsupported CPU.")
        else:
            #Unfortunatelly, have to recreate the dict so it
            #doesn't use the same obj for all cpus
            return {
                'Generic-OoO-1-Issue' : GenericOoO1IssueCPU(),
                'Generic-OoO-2-Issue' : GenericOoO2IssueCPU(),
                'Generic-OoO-3-Issue' : GenericOoO3IssueCPU(),
                'Generic-OoO-4-Issue' : GenericOoO4IssueCPU(),
                'Generic-OoO-5-Issue' : GenericOoO5IssueCPU(),
                'Generic-OoO-6-Issue' : GenericOoO6IssueCPU(),
                "Generic-OoO-4-Issue-with-BT" : GenericOoO4IssueCPU(),
                "A7" : ex5_LITTLE(),
                "A15" : ex5_big()
            }.get(name, 0)


    def getSystem(self, cpu_name, num_cpu):
        sys = BaseSystem()

        l2  = BasicL2Cache()
        l3  = BasicL3Cache()


        cpus = []
        for cpu_id in xrange(int(num_cpu)):
            cpu = self.getCPU(cpu_name)
            cpu.cpu_id = cpu_id
            cpus.append(cpu)
        sys.addCPU(cpus)
        if "Simple" in cpu_name:
            sys.setMemoryMode("atomic")
            for core in sys.cpu:
                l1i = BasicL1Cache()
                l1d = BasicL1Cache()
                sys.addL1(core, l1i, l1d)
        else:
            sys.setMemoryMode("timing")
            #@TODO: Smartly connect caches in multicore env
            if "A7" in cpu_name or "A15" in cpu_name:
                sys.addARMCaches(cpu_name)
            else:
                sys.addL1L2L3(l2, l3)

        if "with-BT" in cpu_name:
            myCgfCache = CfgCache()
            myBT = BTUnit()
            sys.connectBT(myBT)
            sys.connectCfgCache(myCgfCache, myBT)

        sys.completeSetup()

        for core in sys.cpu:
            sys.setupInterrupts(core)

        return sys
