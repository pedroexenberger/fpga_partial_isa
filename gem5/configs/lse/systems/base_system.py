from m5.objects import AddrRange
from m5.objects import DDR3_1600_8x8
from m5.objects import SrcClockDomain
from m5.objects import System
from m5.objects import SystemXBar
from m5.objects import VoltageDomain

from basic_caches import BasicL1Cache

from m5.defines import buildEnv
from m5.objects import FUDesc
from m5.objects import OpDesc

from  ex5_LITTLE import L1I as LITTLEL1I
from  ex5_LITTLE import L1D as LITTLEL1D 
from  ex5_LITTLE import L2  as LITTLEL2
from  ex5_big    import L1I as bigL1I
from  ex5_big    import L1D as bigL1D
from  ex5_big    import L2 as bigL2

class BaseSystem(System):
    def __init__(self, options=None):
        super(BaseSystem, self).__init__()

        self.clk_domain = SrcClockDomain()
        self.clk_domain.voltage_domain = VoltageDomain()
        self.clk_domain.clock = '3.2GHz'

    def setMemoryMode(self, mode):
        if   mode == "timing":
            self.mem_mode = 'timing'
        elif mode == "atomic":
            self.mem_mode = 'atomic'
        else:
            pass
        self.mem_ranges = [AddrRange('512MB')]

    def addCPU(self, CPU):
        self.cpu = CPU

    def addL1(self, cpu, L1ICache, L1DCache):
        self.membus = SystemXBar()
        cpu.icache  = L1ICache
        cpu.dcache  = L1DCache

        cpu.icache.cpu_side = cpu.icache_port
        cpu.icache.mem_side = self.membus.slave
        cpu.dcache.cpu_side = cpu.dcache_port
        cpu.dcache.mem_side = self.membus.slave

    def connectBT(self, bt):
        self.BTUnit = bt

        self.BTUnit.num_rows      = 1024
        self.BTUnit.cycle_size    =    3
        self.BTUnit.num_branches  =    1
        self.BTUnit.num_insts     =    0

        IntALU = FUDesc()
        IntALU.count  = 4
        IntALU.opList = [
            OpDesc(opClass='IntAdd'),
            OpDesc(opClass='IntSub'),
            OpDesc(opClass='IntLogic'),
            OpDesc(opClass='IntShift'),
            OpDesc(opClass='IntMov'),
            OpDesc(opClass='IntCtrl'),
            OpDesc(opClass='IntALU')]

        MulALU = FUDesc()
        MulALU.count = 2
        MulALU.opList = [
            OpDesc(opClass='IntMult', opLat=9)
        ]

        ReadPort = FUDesc()
        # WARNING: this latency must be the same as that of the data cache!!!
        ReadPort.opList = [ OpDesc(opClass='MemRead', opLat=12 ) ]
        ReadPort.count = 2

        WritePort = FUDesc()
        WritePort.opList = [ OpDesc(opClass='MemWrite', opLat=3) ]
        WritePort.count = 1

        self.BTUnit.supported_ops = [ IntALU, MulALU, ReadPort, WritePort ]

        self.BTUnit.num_cores = len(self.cpu)

        for core in self.cpu:
            self.BTUnit.connectCPU  (core)


    def connectCfgCache(self, cfgCache, bt):
        self.cfgCache = cfgCache
        for core in self.cpu:
            self.cfgCache.connectCPU(core)
        self.cfgCache.connectBT(bt)
        self.cfgCache.num_cores = len(self.cpu)


    # def connectDCAModules(self, bt, cfgCache):
    #     self.connectBT(bt)
    #     self.connectCfgCache(cfgCache)

    def addL1L2L3(self, L2Cache, L3Cache):

        # Comm between L1-L2
        self.l2bus  = SystemXBar()
        # Comm betweem L2-L3
        self.l3bus  = SystemXBar()
        # Comm between L3-MainMem
        self.membus = SystemXBar()

        self.l2cache = L2Cache
        self.l3cache = L3Cache

        self.l2cache.cpu_side = self.l2bus.master
        self.l2cache.mem_side = self.l3bus.slave

        self.l3cache.cpu_side = self.l3bus.master
        self.l3cache.mem_side = self.membus.slave

        for core in self.cpu:
            core.icache  = BasicL1Cache()
            core.dcache  = BasicL1Cache()


            core.icache.cpu_side = core.icache_port
            core.icache.mem_side = self.l2bus.slave
            core.dcache.cpu_side = core.dcache_port
            core.dcache.mem_side = self.l2bus.slave

    def addARMCaches(self, cpu_name):
        # Comm between L1-L2
        self.l2bus  = SystemXBar()
        # Comm between L2-MainMem
        self.membus = SystemXBar()

        if "A7" in cpu_name:
            self.l2cache = LITTLEL2()
        else:
            self.l2cache = bigL2()

        self.l2cache.cpu_side = self.l2bus.master
        self.l2cache.mem_side = self.membus.slave

        for core in self.cpu:
            if "A7" in cpu_name:
                core.icache  = LITTLEL1I()
                core.dcache  = LITTLEL1D()
            else:
                core.icache  = bigL1I()
                core.dcache  = bigL1D()


            core.icache.cpu_side = core.icache_port
            core.icache.mem_side = self.l2bus.slave
            core.dcache.cpu_side = core.dcache_port
            core.dcache.mem_side = self.l2bus.slave


    def completeSetup(self):
        self.system_port = self.membus.slave

        self.mem_ctrl = DDR3_1600_8x8()
        self.mem_ctrl.range = self.mem_ranges[0]
        self.mem_ctrl.port = self.membus.master

        return self

    def setupInterrupts(self, cpu):

        cpu.createInterruptController()
        if buildEnv['TARGET_ISA'] == 'x86':
            cpu.interrupts[0].pio = self.membus.master
            cpu.interrupts[0].int_master = self.membus.slave
            cpu.interrupts[0].int_slave = self.membus.master
