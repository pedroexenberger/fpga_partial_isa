# -*- coding: utf-8 -*-

import m5
from m5.objects import *

from m5.objects import BaseCache
from m5.objects import DDR3_1600_8x8
from m5.objects import DerivO3CPU
from m5.objects import System
from m5.objects import SystemXBar

###############################################################################

## Unidades funcionais
##
## Cada classe especifica um tipo de unidade funcional.
##
## O campo opList especifica os tipos de operação que a FU executa e o campo
## count especifica a quantidade de unidades desse tipo.

###############################################################################


class MyIntALU(FUDesc):
    opList = [ OpDesc(opClass='IntAdd'),
               OpDesc(opClass='IntSub'),
               OpDesc(opClass='IntMov'),
               OpDesc(opClass='IntLogic'),
               OpDesc(opClass='IntShift'),
               OpDesc(opClass='IntCtrl'),
               OpDesc(opClass='IntALU') ]
    count = 4

class MyIntMultDiv(FUDesc):
    opList = [ OpDesc(opClass='IntMult', opLat=3),
               OpDesc(opClass='IntDiv', opLat=20, pipelined=False) ]

    # DIV and IDIV instructions in x86 are implemented using a loop which
    # issues division microops.  The latency of these microops should really be
    # one (or a small number) cycle each since each of these computes one bit
    # of the quotient.
    if buildEnv['TARGET_ISA'] in ('x86'):
        opList[1].opLat=1

    count = 2

class My_FP_ALU(FUDesc):
    opList = [ OpDesc(opClass='FloatAdd', opLat=2),
               OpDesc(opClass='FloatCmp', opLat=2),
               OpDesc(opClass='FloatCvt', opLat=2) ]
    count = 2

class My_FP_MultDiv(FUDesc):
    opList = [ OpDesc(opClass='FloatMult', opLat=4),
               OpDesc(opClass='FloatDiv', opLat=12, pipelined=False),
               OpDesc(opClass='FloatSqrt', opLat=24, pipelined=False) ]
    count = 1

class My_SIMD_Unit(FUDesc):
    opList = [ OpDesc(opClass='SimdAdd'),
               OpDesc(opClass='SimdAddAcc'),
               OpDesc(opClass='SimdAlu'),
               OpDesc(opClass='SimdCmp'),
               OpDesc(opClass='SimdCvt'),
               OpDesc(opClass='SimdMisc'),
               OpDesc(opClass='SimdMult'),
               OpDesc(opClass='SimdMultAcc'),
               OpDesc(opClass='SimdShift'),
               OpDesc(opClass='SimdShiftAcc'),
               OpDesc(opClass='SimdSqrt'),
               OpDesc(opClass='SimdFloatAdd'),
               OpDesc(opClass='SimdFloatAlu'),
               OpDesc(opClass='SimdFloatCmp'),
               OpDesc(opClass='SimdFloatCvt'),
               OpDesc(opClass='SimdFloatDiv'),
               OpDesc(opClass='SimdFloatMisc'),
               OpDesc(opClass='SimdFloatMult'),
               OpDesc(opClass='SimdFloatMultAcc'),
               OpDesc(opClass='SimdFloatSqrt') ]
    count = 1

class MyReadPort(FUDesc):
    opList = [ OpDesc(opClass='MemRead') ]
    count = 2

class MyWritePort(FUDesc):
    opList = [ OpDesc(opClass='MemWrite') ]
    count = 1

class MyIprPort(FUDesc):
    opList = [ OpDesc(opClass='IprAccess', opLat = 3, pipelined = False) ]
    count = 1

class MyFUPool(FUPool):
    FUList = [ MyIntALU(), MyIntMultDiv(), My_FP_ALU(),
               My_FP_MultDiv(), MyReadPort(),
               MyWritePort(), My_SIMD_Unit(), MyIprPort() ]


############################################################

## Processador

############################################################

class GenericOoO1IssueCPU(DerivO3CPU):

############################################################
## Preditor de desvios
############################################################
    branchPred = TournamentBP() # Branch Predictor

############################################################
## Latências entre os diferentes estágios do pipeline.
## Pode ser usado para simular pipelines mais profundos.
############################################################
    #### Latências de avanço
    fetchToDecodeDelay   = 1 # Fetch to decode delay
    decodeToRenameDelay  = 1 # Decode to rename delay
    renameToIEWDelay     = 1 # Rename to Issue/Execute/Writeback delay
    renameToROBDelay     = 1 # Rename to reorder buffer delay
    issueToExecuteDelay  = 1 # Issue to execute delay internal to the IEW stage
    iewToCommitDelay     = 1 # Issue/Execute/Writeback to commit delay

    #### Latências de retorno
    decodeToFetchDelay   = 1 # Decode to fetch delay

    renameToFetchDelay   = 1 # Rename to fetch delay
    renameToDecodeDelay  = 1 # Rename to decode delay

    iewToFetchDelay      = 1 # Issue/Execute/Writeback to fetch delay
    iewToDecodeDelay     = 1 # Issue/Execute/Writeback to decode delay
    iewToRenameDelay     = 1 # Issue/Execute/Writeback to rename delay

    commitToFetchDelay   = 1 # Commit to fetch delay
    commitToDecodeDelay  = 1 # Commit to decode delay
    commitToRenameDelay  = 1 # Commit to rename delay
    commitToIEWDelay     = 1 # Commit to Issue/Execute/Writeback delay

############################################################
## Tamanho das estruturas do pipeline. Afetam a quantidade
## de instruções que podem ser armazenadas nos buffers.
############################################################

    fetchBufferSize  =  64 # Fetch buffer size in bytes
    fetchQueueSize   = 256 # Fetch queue size in micro-ops per thread
    numIQEntries     = 256 # Number of instruction queue entries
    numROBEntries    = 256 # Number of reorder buffer entries
    LQEntries        = 128 # Number of load queue entries
    SQEntries        = 128 # Number of store queue entries

    # It will break with any number smaller than this
    numPhysIntRegs   = 512 # Number of physical integer registers
    numPhysFloatRegs = 512 # Number of physical floating point registers

    numRobs          =  1 # Number of Reorder Buffers;

############################################################
## Largura das estruturas do pipeline. Afetam a quantidade
## de instruções processadas por ciclo em cada estágio.
############################################################

    fetchWidth    =  1 # Fetch width
    decodeWidth   =  1 # Decode width
    renameWidth   =  1 # Rename width
    dispatchWidth =  1 # Dispatch width
    issueWidth    =  1 # Issue width
    wbWidth       =  1 # Writeback width
    commitWidth   =  1 # Commit width
    squashWidth   = 16 # Squash width

    fuPool = MyFUPool() # Functional Unit pool

############################################################
## Outros parâmetros. Sugestão: não mexer.
############################################################

    LSQDepCheckShift = 4 # Number of places to shift addr before check
    LSQCheckLoads = True # Should dependency violations be checked for
                         # loads & stores or just stores
    store_set_clear_period = 250000 # Number of load/store insts before
                                    # the dep predictor should be invalidated
    LFSTSize = 1024 # Last fetched store table size
    SSITSize = 1024 # Store set ID table size

    # most ISAs don't use condition-code regs # so default is 0
    _defaultNumPhysCCRegs = 0

    # For x86, each CC reg is used to hold only a subset of the flags, so we
    # need 4-5 times the number of CC regs as physical integer regs to be
    # sure we don't run out.  In typical real machines, CC regs are not
    # explicitly renamed (it's a side effect of int reg renaming),
    # so they should never be the bottleneck here.
    _defaultNumPhysCCRegs = numPhysIntRegs * 5
    numPhysCCRegs = _defaultNumPhysCCRegs # Number of physical cc registers

    activity = 0 # Initial count

    cacheStorePorts = 1 # Cache Ports

    trapLatency = 10 # Trap latency
    fetchTrapLatency = 1 # Fetch trap latency

    backComSize = 32 # Time buffer size for backwards communication
    forwardComSize = 32 # Time buffer size for forward communication

    smtNumFetchingThreads = 1 # SMT Number of Fetching Threads
    smtFetchPolicy = 'SingleThread' # SMT Fetch policy
    smtLSQPolicy = 'Partitioned' # SMT LSQ Sharing Policy
    smtLSQThreshold = 100 # SMT LSQ Threshold Sharing Parameter
    smtIQPolicy = 'Partitioned' # SMT IQ Sharing Policy
    smtIQThreshold = 100 # SMT IQ Threshold Sharing Parameter
    smtROBPolicy = 'Partitioned' # SMT ROB Sharing Policy
    smtROBThreshold = 100 # SMT ROB Threshold Sharing Parameter
    smtCommitPolicy = 'RoundRobin' # SMT Commit Policy

    needsTSO = True # Enable TSO Memory model
