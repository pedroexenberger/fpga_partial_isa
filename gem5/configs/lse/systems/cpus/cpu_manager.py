import NIssue
import HaswellLike
import NehalemLike
import SandyBridgeLike

import re

class CPUManager:
    available_cpus = [
        "OoO,N-Issue (N in [1, 6])",

        "Nehalem-Like (6-issue)",
        "Sandy-Bridge-Like (7-issue)",
        "Haswell-Like (8-issue)"]


    def getCPU(name):
        # Test if N-Issue processor
        match = re.search(name, "^OoO,[1-6]-Issue$")
        if match:
            match = re.search("[1-6]")
            num   = match.group()

            return

        # Tests if other processors
        if   name == "Nehalem-Like":
        elif name == "Sandy-Bridge-Like":
        elif name == "Haswell-Like":
