# -*- coding: utf-8 -*-

from __future__     import print_function

from subprocess import *

# Beautiful COLors for formatting
class bcol:
    MAGENTA    = '\033[95m'
    BLUE       = '\033[94m'
    GREEN      = '\033[92m'
    YELLOW     = '\033[93m'
    RED        = '\033[91m'
    BOLD       = '\033[1m'
    UNDERLINE  = '\033[4m'
    END        = '\033[0m'

    @staticmethod
    def H(msg):
        return bcol.MAGENTA + msg + bcol.END

    @staticmethod
    def I(msg):
        return bcol.BLUE + msg + bcol.END

    @staticmethod
    def W(msg):
        return bcol.YELLOW + msg + bcol.END

    @staticmethod
    def OK(msg):
        return bcol.GREEN + msg + bcol.END

    @staticmethod
    def E(msg):
        return bcol.RED + msg + bcol.END


print(bcol.H( \
"***************************************************************************"))
print(bcol.H( \
"******************************* Run benchmarks ****************************"))
print(bcol.H( \
"****************************** run_benchmarks.py **************************"))
print(bcol.H( \
"***************************************************************************"))
print("")
print("")
print("")
print("")

print(bcol.I( \
"Executing gem5 for:"))
#benchmarks = ["basicmath","bitcount","blowfish-e",
#      "CRC32","dijkstra","FFT","FFT-i","gsm-d","gsm-e",
#      "jpeg-d","jpeg-e","patricia","qsort", "rsynth", "sha","stringsearch",
#     "susan-c","susan-e","susan-s"]

benchmarks = ["correlation", "gemm", "gesummv", "2mm", "3mm",
        "cholesky", "durbin", "gramschmidt", "ludcmp", "floyd-warshall",
        "jacobi-1d"]
sizes = ["small"]
processors = ["A7", "A15"]
isa = "RISCV"

for processor in processors:
    print(bcol.I("	%s processor" % processor))
    for size in sizes:
        print(bcol.I( "	    %s size" % size))
        for benchmark in benchmarks:
            print(bcol.I( "         %s benchmark" % benchmark))
            debug_file  = "--debug-file=%s%s.out" % (benchmark,processor)
            stats_file  = "--stats-file=%s_%s_%s.txt" % (benchmark,size,processor)
            dump_config = "--dump-config=%s_%s_%s.ini" % (benchmark,size,processor)
            json_config = "--json-config=%s_%s_%s.json" % (benchmark,size,processor)


            cmd_call = "./build/%s/gem5.opt --debug-flags=FloatMark \
            %s %s %s %s --redirect-stdout --redirect-stderr configs/lse/simulate.py \
            --cpu=%s run-other --cmd='%s'" \
            % (isa, debug_file,stats_file,dump_config,json_config,processor,benchmark)
            call([cmd_call], shell=True)



