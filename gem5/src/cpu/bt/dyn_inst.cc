// -*- c-file-style: "m5"; -*-

#include "dyn_inst.hh"

BTDynInst::BTDynInst(StaticInstPtr inst, TheISA::PCState pc,
                     Addr effAddr, bool isTimingMemory, unsigned cpuId) :
    _pc(pc), _staticInst(inst),  _effAddr(effAddr),
    _isTimingMemory(isTimingMemory), _cpuId(cpuId)
{
}

StaticInstPtr
BTDynInst::staticInst()
{
    return _staticInst;
}

TheISA::PCState
BTDynInst::pcState()
{
    return _pc;
}

Addr
BTDynInst::effAddr()
{
    return _effAddr;
}

bool
BTDynInst::isTimingMemory()
{
    return _isTimingMemory;
}

unsigned
BTDynInst::cpuId()
{
    return _cpuId;
}
