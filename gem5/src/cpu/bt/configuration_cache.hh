// -*- c-file-style: "m5"; -*-

#pragma once

#include <list>
#include <queue>

#include "base/statistics.hh"
#include "cpu/bt/comm/ports.hh"
#include "cpu/bt/ra_configuration.hh"
#include "mem/mem_object.hh"
#include "params/CfgCache.hh"

/** Declare this hash function for TheISA::PCState so that STL knows how to
    generate an unordered map of TheISA::PCState. */
namespace std {
template<>
struct hash<CGRAPCState>
{
    size_t operator () (const CGRAPCState& x) const
    {
        return std::hash<int>()(x.pc());
    }
};
}

/** Support class for the priority queue implementing the LRU
    cache configuration replacement policy.

    Implements the comparison operator required to compare two
    configurations. */
class CompareConfigs
{
  public:
    bool operator()
    (RAConfigurationPtr a, RAConfigurationPtr b) {
        return a->useCounter > b->useCounter;
    }
};

class CfgCache : public MemObject
{
  private:

        /** Receives requests from the CPU. */
    std::vector<CfgCache2CPUPort*> cpuPorts;
    /** Receives requests from the BT. */
    CfgCache2BTPort btPort;

    Stats::Scalar cfgLookup;
    Stats::Scalar cfgLookupHits;

  public:

    unsigned int size;
    unsigned int numCores;

    /** Must keep synchronized */
    std::unordered_map<CGRAPCState, RAConfigurationPtr> cfgMap;

    /** For keeping track of LRU. */
    std::priority_queue <RAConfigurationPtr,
                         std::vector<RAConfigurationPtr>,
                         CompareConfigs> cfgPriorityQueue;

    /* Cached values */
    RAConfigurationPtr cachedLookup;

    CfgCache(CfgCacheParams*);
    ~CfgCache();

    bool saveConfiguration(RAConfigurationPtr);
    bool lookupConfiguration(CGRAPCState);

    void eraseCfg(RAConfigurationPtr);

    RAConfigurationPtr lastLookup();

    void regStats() override;

  public:
    BaseMasterPort& getMasterPort(const std::string& if_name,
                                  PortID idx = InvalidPortID) override;

    BaseSlavePort&  getSlavePort(const std::string& if_name,
                                 PortID idx = InvalidPortID) override;
};
