// -*- c-file-style: "m5"; -*-
#include <climits>
#include <iomanip>

#include "cpu/bt/bt.hh"
#include "debug/BT.hh"
#include "ra_configuration.hh"

#define TEST_ALUOP(X)                           \
    ((X) == IntAddOp   ||                       \
     (X) == IntSubOp   ||                       \
     (X) == IntLogicOp ||                       \
     (X) == IntShiftOp ||                       \
     (X) == IntMovOp   ||                       \
     (X) == IntCtrlOp  ||                       \
     (X) == IntALUOp)

bool RAConfiguration::supported(const StaticInstPtr& inst)
{
    for (auto row_operations : row_fu) {
        OpDesc* fu = RAConfiguration::findOpClass(row_operations,
                                                  inst->opClass());
        if (fu != nullptr){
            return true;
        }
    }
    return false;
}

bool RAConfiguration::RARow::canAddInstr(BTDynInstPtr const &inst) const {
    unsigned row_fu_idx = 0;
    /*
     * In the list of FUs types of the row,
     * find one capable of executing the
     * instruction. If found, check if
     * there is a free FU of that type
     */
    for (auto row_operations : parent->row_fu) {
        OpDesc* fu =
          RAConfiguration::findOpClass(row_operations,
                                       inst->staticInst()->opClass());
        if (fu != nullptr){
            unsigned fu_latency;
            if (TEST_ALUOP(inst->staticInst()->opClass())) {
                fu_latency = fu->opLat;
            }else{
                fu_latency = parent->cycleSize;
            }
            auto fu_cols_per_row = row_operations->number;
            if (index % fu_latency == 0 &&
                RAConfiguration::RARow::used_fu_cols_per_row[row_fu_idx] <
                fu_cols_per_row) {
                return true;
            }
        }
        ++row_fu_idx;
    }

    return false;
}

typename RAConfiguration::RAInst*
RAConfiguration::RARow::addInstr(const BTDynInstPtr &inst, unsigned i) {
    assert(canAddInstr(inst));

    // If not (isControl) then cols.size < cols.capacity
    assert(inst->staticInst()->isControl() || cols.size() < cols.capacity());

    /* Builds the RAInst in the row */
    cols.emplace_back(this, inst->staticInst(), i);
    RAInst* raInst = &cols[numColsUsed()];

    /* Sets some parameters of the RAInst */
    raInst->completionRow   = this->index +
        RAConfiguration::RARow::getOPLatency(inst) -1;
    raInst->completionLevel = raInst->completionRow / parent->cycleSize;
    raInst->pc              = inst->pcState();

    allocFU(inst);

    DPRINTF(BT, "Mapped %s.\n", cols.back().getStr());

    return raInst;
}

unsigned int
RAConfiguration::RARow::getOPLatency(const BTDynInstPtr &inst)
{
    /*
     * In the list of FUs types of the row,
     * find one capable of executing the given
     * instruction. If type found,
     * return the latency of the operation
     */
    for (auto row_operations : parent->row_fu) {
        OpDesc* fu = RAConfiguration::findOpClass(
          row_operations,
          inst->staticInst()->opClass());
        if (fu != nullptr)
            return fu->opLat;
    }
    return UINT_MAX;
}

void
RAConfiguration::RARow::allocFU(const BTDynInstPtr &inst)
{
    /*
     * For each FU type, find the one able to execute
     * the given instruction.
     * When found, increase the allocation counter.
     */
    unsigned row_fu_idx = 0;
    for (auto row_operations : parent->row_fu){
        OpDesc* fu = RAConfiguration::findOpClass(
          row_operations,
          inst->staticInst()->opClass());
        if (fu != nullptr){
            ++RAConfiguration::RARow::used_fu_cols_per_row[row_fu_idx];
        }
        ++row_fu_idx;
    }
}

void
RAConfiguration::RARow::deallocFU(const StaticInstPtr &inst)
{
    /*
     * For each FU type, find the one able to execute
     * the given instruction.
     * When found, decrease the allocation counter.
     */
    unsigned row_fu_idx = 0;
    for (auto row_operations : parent->row_fu){
        OpDesc* fu = RAConfiguration::findOpClass(
          row_operations,
          inst->opClass());
        if (fu != nullptr &&
            RAConfiguration::RARow::used_fu_cols_per_row[row_fu_idx] > 0){
            --RAConfiguration::RARow::used_fu_cols_per_row[row_fu_idx];
        }
        ++row_fu_idx;
    }
}

std::string RAConfiguration::RAInst::getStr()
{
    std::ostringstream str;
    str << "\"";
    str << index;
    str << ": ";

    /** OP Name */
    str << staticInst->getName();

    /** Input regs */
    int i;
    int srcRegs = staticInst->numSrcRegs();
    str << "(src: ";
    for (i = 0; i < srcRegs-1; i++) {
        str << "R" << staticInst->srcRegIdx(i) << ",";
    }
    if (srcRegs > 0)
        str << "R" << staticInst->srcRegIdx(i);

    /** Output regs */
    int destRegs = staticInst->numDestRegs();
    str << " dst: ";
    for (i = 0; i < destRegs-1; i++) {
        str << "R" << staticInst->destRegIdx(i) << ",";
    }
    if (destRegs > 0)
        str << "R" << staticInst->destRegIdx(i) << ")";

    /** Row/Level */
    str << " Rdy @ Row" << completionRow+1 << " Lev" <<
        completionLevel+1 << "\"";

    return str.str();
}

RAConfiguration::RAInst::RAInst(
    RAConfiguration::RARow* row, StaticInstPtr inst, unsigned index) :
    parentCfg(row->parent),
    parentRow(row),
    staticInst(inst),
    index(index)
{

}

bool RAConfiguration::canAddInstr(BTDynInstPtr const &inst) {
    StaticInstPtr staticInst = inst->staticInst();

    /* Row where this op will be scheduled. */
    unsigned int rowIndex = 0;
    bool found_dep = false;

    for (int i = 0; i < staticInst->numSrcRegs(); i++) {
        RegId inDep = staticInst->srcRegIdx(i);

        auto depRow = dep_table.find(inDep);

        if (depRow != dep_table.end()) {
            rowIndex = std::max(rowIndex, depRow->second->index);
            found_dep = true;
        }
    }

    /*TODO: Check BTDynInst::_isTimingMemory definition*/
    if (inst->isTimingMemory()){
        /** If it is a memory op, check for memory dependence ordering. We
         * assume that value forwarding is possible to the same row. */
        if (staticInst->opClass() == MemReadOp) {
            switch (memOrderPolicy) {
              case FullInOrder: // Do not reorder with previous loads or stores
                rowIndex = std::max(rowIndex, last_load_row);
                rowIndex = std::max(rowIndex, last_store_row);
                break;
              case InOrder: // Do not reorder with stores
                rowIndex = std::max(rowIndex, last_store_row);
                break;
              case SimplePred: { // Allow load to precede a previous store, if
                  // no collision is found.
                  auto found = storeTbl.find(inst->effAddr());
                  if (found != storeTbl.end()) {
                      rowIndex = std::max(rowIndex, found->second);
                  }
              } break;
              case None:
                break;
            }
        }
        else if (staticInst->opClass() == MemWriteOp) {
            switch (memOrderPolicy) {
              case FullInOrder: // Do not reorder with previous loads or stores
              case InOrder:
              case SimplePred:
                rowIndex = std::max(rowIndex, last_load_row);
                rowIndex = std::max(rowIndex, last_store_row);
                break;
              case None:
                break;
            }
        }
    }

    /* New instruction goes on first row after dep is complete. */
    if (found_dep)
        rowIndex++;
    if (rowIndex >= maxRows)
        return false;

    rowsIt = rows.begin() + rowIndex;

    bool found_slot = false;
    /* Forwards search for available functional units. */
    while (rowsIt != rows.end() && !found_slot) {

        if (rowsIt->canAddInstr(inst)) {
            found_slot = true;
        } else {
            rowsIt++;
        }
    }

    return found_slot;
}

RAConfiguration::RAConfiguration(BTUnit& bt) :
    generator(bt),
    /* Parameters */
    row_fu(bt.supportedOps),
    memOrderPolicy(bt.memOrderPolicy),
    cycleSize(bt.cycleSize),
    maxInsts(bt.maxInsts),
    maxRows(bt.maxRows),
    maxBranches(bt.maxBranches),

    mapped_instructions(0),
    mapped_blocks(0),
    mapped_alu_instructions(0),
    mapped_load_instructions(0),
    mapped_store_instructions(0),
    cycles(0),
    dynLock(false),
    last_load_row(0),
    last_store_row(0),
    eip_start(maxBranches, 0),
    eip_end(maxBranches, 0),
    dynMisspecCount(0),
    useCounter(0)
{
    rows.reserve(maxRows);
    for (unsigned int i = 0; i < maxRows; i++) {
        rows.emplace_back(RARow(this, i));
    }
    ordered_insts.reserve(maxInsts); // @TODO

}

bool RAConfiguration::addInstr(BTDynInstPtr const &inst) {
    StaticInstPtr staticInst = inst->staticInst();

    /** Tests if can add this instruction, and updates rowsIt
     *  to point to the row where it will be allocated. */
    if (!canAddInstr(inst))
        return false;
    RAInst* raInst = rowsIt->addInstr(inst, mapped_instructions);
    assert(raInst != nullptr);

    /* Insert instruction in this configuration's ordered op list. */
    ordered_insts.push_back(raInst);

    /** Updates dep_table */
    for (int i = 0; i < staticInst->numDestRegs(); i++) {
        RegId dep = staticInst->destRegIdx(i);

        /* We test if this dep is already indexed in the table,
         * removing this entry (if it exists) and writing the new one.
         */
        auto row = dep_table.find(dep);
        if (row != dep_table.end()) {
            dep_table.erase(row);
        }

        RARow* completionRow;
        if (raInst->completionRow < maxRows)
            completionRow = &rows[raInst->completionRow];
        else
            completionRow = &rows[maxRows-1];

        dep_table.emplace(dep, completionRow);
    }

    /** Updates reference to last row with a store, and table
     *  that monitors load-store collision. */
    if (staticInst->opClass() == MemWriteOp) {
        auto found = storeTbl.find(inst->effAddr());
        if (found != storeTbl.end()) {
            storeTbl.erase(found);
        }
        storeTbl.emplace(inst->effAddr(), raInst->parentRow->index);

        last_store_row = rowsIt->index; // Stores complete in one cycle
    }
    if (staticInst->opClass() == MemReadOp) {
        last_load_row = rowsIt->index;
    }

    /** completionLevel+1, to account for 0-based indexes. */
    cycles = std::max(cycles, raInst->completionLevel+1);

    /** Updates live-ins and live-outs of this configuration */
    for (int i = 0; i < staticInst->numSrcRegs(); i++) {
        RegId reg = staticInst->srcRegIdx(i);

        if (staticLiveOuts.count(reg)) {
            /** Reg was produced inside the configuration, therefore
             * it is not a live-in. */
        } else {
            staticLiveIns.insert(reg);
        }
    }

    for (int i = 0; i < staticInst->numDestRegs(); i++) {
        RegId reg = staticInst->destRegIdx(i);
        staticLiveOuts.insert(reg);
    }

    /*
      @TODO: Better statistics with array 2.0 new structure
    */
    /* Statistics */
    mapped_instructions++;
    if (TEST_ALUOP(inst->staticInst()->opClass())) {
        mapped_alu_instructions++;
    } else if (inst->staticInst()->opClass() == IntMultOp) {
        mapped_mult_instructions++;
    } else if (inst->staticInst()->opClass() == MemReadOp) {
        mapped_load_instructions++;
    } else if (inst->staticInst()->opClass() == MemWriteOp) {
        mapped_store_instructions++;
    }

    return true;
}

void RAConfiguration::printAsDot(std::ostream &os) const {
    os << std::dec << "digraph G {\n";
    os << "\trankdir = BT;\n";
    os << "\tratio = 0.4;\n";

    int i = 0; /* Row index */
    int j = 0; /* Instruction order */
    int k = 0;
    int l = 0;

    /* Prints all nodes */
    for (auto& rowsIt : rows) {
        if (i % cycleSize == 0) {
            os << "\tsubgraph cluster_" << i << " {\n";
            os << "\t\tcolor=blue;\n";
            os << "\t\tlabel=\"Level #" << i / cycleSize + 1 << "\";\n";
            os << "\t\tnode[style=filled, color=grey, shape=rectangle];\n";
        }

        /* ALU operations */
    {
        os << "\n\t\tsubgraph cluster_ALU_" << i+1 << "{\n";
        os << "\t\t\tcolor=red;\n";
        os << "\t\t\tlabel=\"Row #" << i+1 << "\";\n";
        os << "\t\t\tnode[style=filled, color=grey, shape=rectangle];\n\n";

        for (auto& instrIt : rowsIt.getCols()) {
            if (TEST_ALUOP(instrIt.staticInst->opClass())) {
                os << "\t\t\t" << instrIt.getStr() << ";\n";
            }
        }

        os << "\n\t\t}\n\n";
    }

    /* Memory operations */
{
    for (auto& instrIt : rowsIt.getCols()) {
        OpClass cls = instrIt.staticInst->opClass();
        if (cls == MemReadOp || cls == MemWriteOp) {
            os << "\t\t" << instrIt.getStr() << ";\n";
        }
    }
}

if (i % cycleSize == cycleSize-1) {
    os << "\t}\n";
}

i++;
    }

    if (i % cycleSize != 0) {
        os << "\t}\n";
    }

    for (i = rows.size() - 1; i > 0; i--) {
        const RARow *row = &rows[i];

        if (row->getCols().empty()) continue;

        for (j = row->numColsUsed()-1; j >= 0; j--) {

            RAInst instr = row->getCols()[j];

            for (k = i-1; k >= 0; k--) {
                const RARow *auxRow = &rows[k];

                if (auxRow->getCols().empty()) continue;

                for (l = auxRow->numColsUsed()-1; l >= 0; l--) {

                    RAInst auxInstr = auxRow->getCols()[l];

                    if (hasRegRAW(auxInstr.staticInst, instr.staticInst) &&
                        auxInstr.getIndex() < instr.getIndex()) {
                        os << "\t" << auxInstr.getStr() << " -> " <<
                            instr.getStr() << ";\n";
                        break;
                    }

                }
            }

        }
    }

    os << "node[style=filled, color=green, shape=rectangle];\n";
    os << "uILP [label=\"ILP = " << getILP() << "\"];\n";
    os << "Microops [label=\"Instructions = " <<  std::dec <<
        getInstructions() << "\"];\n";
    os << "Cycles [label=\"Cycles = " << std::dec << cycles << "\"];\n";
    os << "EIP [label=\"EIP = 0x" << std::hex <<
        std::setfill('0') << std::setw(8) << getPCState().pc() << "\"];\n";


    os << "}";
}

bool RAConfiguration::hasRegRAW(StaticInstPtr inst1, StaticInstPtr inst2)
{
    for (int i = 0; i < inst2->numSrcRegs(); i++) {
        RegId thisInput = inst2->srcRegIdx(i);

        for (int j = 0; j < inst1->numDestRegs(); j++) {
            RegId thatOutput = inst1->destRegIdx(j);

            if (thisInput == thatOutput) {
                return true;
            }
        }
    }

    return false;
}

float RAConfiguration::getILP() const
{
    assert(cycles > 0);
    return (float) this->mapped_instructions / cycles;
}

unsigned RAConfiguration::getInstructions() const
{
    return this->mapped_instructions;
}

CGRAPCState RAConfiguration::getPCState() const
{
    return eip_start[0];
}

bool RAConfiguration::empty() const
{
    return (mapped_instructions == 0);
}

void RAConfiguration::checkCorrectness()
{

    assert(mapped_instructions == ordered_insts.size());

    /* Check for the following invariants:
       - ordered_insts is a list of microops partitioned into blocks,
         with the boundaries defined by the vectors eip_start and eip_end.
       - the first microop in each block must have the isFirst flag
       - the last microop in each block, except in the last, must have the
         isLast flag.
    */

    int instId = 0;
    for (int block = 0; block < mapped_blocks; block++) {
        assert(eip_start[block].pc() <= eip_end[block].pc());
        bool readFirst = false;
        bool readLast  = false;

        while (!readLast) {
            RAInst *inst    = ordered_insts[instId];
            CGRAPCState &pc = inst->pc;

            if (pc == eip_start[block]) {
                assert(!readFirst && !readLast);
                readFirst = true;
            }

            assert(pc >= eip_start[block] && pc <= eip_end[block]);

            if (pc == eip_end[block]) {
                assert(readFirst && !readLast);
                readLast = true;
            }

            assert(instId < mapped_instructions);
            instId++;
        }

    }
}

RAConfiguration::RARow::RARow(RAConfiguration *cfg, unsigned int i)
    : parent(cfg),
      index(i)
{

    unsigned fu_types = 0;
    unsigned num_cols = 0;

    for (auto fu: cfg->row_fu){
        num_cols += fu->number;
        ++fu_types;
    }

    cols.reserve(num_cols);
    used_fu_cols_per_row.resize(fu_types,0);
}

void RAConfiguration::undoLastMap()
{
    RAInst *inst = ordered_insts.back();
    RARow   *row = inst->parentRow;

    row->deallocFU(inst->staticInst);

    mapped_instructions--;

    DPRINTF(BT, "Poping inst from row: %s", row->cols.back().getStr());
    row->cols.pop_back();
    DPRINTF(BT, "Poping inst from ordered buffer: %s",
            ordered_insts.back()->getStr());
    ordered_insts.pop_back();

     //unsigned is always >= 0
    //assert(mapped_instructions >= 0);
    if (mapped_instructions != 0) {
        inst = ordered_insts.back();
        unsigned curr_block = mapped_blocks-1;

        /* If this condition is true, then the last mapped block has been
           completely wiped, and mapped_blocks needs to be decremented.
           Otherwise, the last block still exists and we just need to update
           its eip_end. */
        StaticInstPtr &sinst = inst->staticInst;
        if (sinst->isControl() && sinst->isLastMicroop()) {
            mapped_blocks--;
        } else {
            eip_end[curr_block] = inst->pcState();
        }
    }
}

OpDesc*
RAConfiguration::findOpClass(FUDesc *fu, OpClass op)
{
    for (auto fu_op: fu->opDescList){
        if (op==fu_op->opClass)
            return fu_op;
    }
    return nullptr;
}

//#include "cpu/o3/isa_specific.hh"

//template class RAConfiguration<O3CPUImpl>;
