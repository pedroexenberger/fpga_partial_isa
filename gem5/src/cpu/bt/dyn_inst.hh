#pragma once

#include "base/refcnt.hh"
#include "base/types.hh"
#include "config/the_isa.hh"
#include "cpu/static_inst.hh"

/** A StaticInst with tracking of PC state for the BT unit. */
class BTDynInst : public RefCounted
{
    friend class BTUnit;

    TheISA::PCState _pc;
    StaticInstPtr _staticInst;
    Addr _effAddr;
    /*TODO: Probably this should not be here.
    * There should be a check somewhere to avoid the usage
    * of memory reorder policies when no timing is used
    */
    bool _isTimingMemory;
    unsigned _cpuId;

  public:
    BTDynInst(StaticInstPtr inst, TheISA::PCState pc,
                Addr _effAddr, bool _isTimingMemory, unsigned cpuId);

    StaticInstPtr staticInst();
    TheISA::PCState pcState();
    Addr effAddr();
    bool isTimingMemory();
    unsigned cpuId();


    static bool hasRAW(StaticInstPtr inst_r, StaticInstPtr inst_w);
};
typedef RefCountingPtr<BTDynInst> BTDynInstPtr;
