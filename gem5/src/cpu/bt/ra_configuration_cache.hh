#ifndef __RACC_HH__
#define __RACC_HH__

#include <list>

#include "base/statistics.hh"
#include "cpu/bt/ra_configuration.hh"

class RAConfigurationCache {
public:

        typedef typename RAConfiguration RACfg;

        unsigned int cache_size;

        /** Must keep synchronized */
        std::unordered_map<Addr, RACfg*> cfgMap;

        /* Cached values */
        RACfg* cachedLookup;

        RAConfigurationCache(BTUnitParams *params);
        ~RAConfigurationCache();

        void saveConfiguration(RACfg*);
        bool lookupConfiguration(Addr);

        void eraseCfg(RACfg*);

        RAConfiguration* lastLookup();

        void regStats();

        std::string name() const;

        Stats::Scalar cfgsWritten;
        Stats::Scalar cfgLookup;
        Stats::Scalar cfgLookupHits;
        Stats::Scalar cfgReusedInsts;
        Stats::Scalar cfgErase;
        Stats::Scalar cfgEraseUseless;
};

#endif
