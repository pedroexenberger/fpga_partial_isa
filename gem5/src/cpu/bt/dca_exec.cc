//   -*- c-file-style: "m5"; -*-
/*
 * Copyright (c) 2018 The Embedded Systems Group, Institute of Informatics,
 *                    Federal University of Rio Grande do Sul (UFRGS).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: Marcelo Brandalero
 */

#include "cpu/bt/dca_exec.hh"
#include "cpu/o3/dyn_inst.hh"
#include "mem/cache/cache.hh"

void
DCAO3FetchProbe::regStats()
{
    DCACfgsExecuted
        .init(3)
        .name(cpu->cpuId() + ".DCAO3FetchProbe.DCACfgsFetched")
        .desc("Number of DCA configurations fetched")
        .subname(0, "Fully")
        .subdesc(0, "Configurations completely fetched")
        .subname(1, "Partially")
        .subdesc(1, "Configurations with fetch process interrupted")
        .flags(Stats::nozero | Stats::nonan);

    DCAOpsExecuted
        .name(cpu->cpuId() + ".DCAO3FetchProbe.DCAOpsFetched")
        .desc("Number of DCA ops fetched")
        .flags(Stats::nozero | Stats::nonan);
}

DCAO3FetchProbe::DCAO3FetchProbe(BaseCPU *cpu) : cpu(cpu)
{
    regStats();
}

void
DCAO3FetchProbe::probe(DynInstPtr inst)
{
    if (!inst)
        return;

    CPU2CfgCachePort &cpuToCfgCachePort =
        cpu->cpuToCfgCachePort;

    CGRAPCState pc;
    pc = inst->pcState();

    if (!fetchingFromCfgCache) {

        currCfg = cpuToCfgCachePort.lookup(pc);

        /** If a configuration exists for that address,
            start the fetch process. */
        if (currCfg) {
            indexCurrentBB             = 0;
            indexCurrentInst           = 0;

            fetchingFromCfgCache       = true;

            /* Update DCA info inside instruction. */
            inst->dcaInfo.raInst       = true;
            inst->dcaInfo.firstFromRA  = true;
            inst->dcaInfo.levelInRA    =
                currCfg->ordered_insts[indexCurrentInst]
                ->completionLevel;

            DCAOpsExecuted++;
        }
    } else {
        /* Update DCA info inside instruction. */
        inst->dcaInfo.raInst       = true;

        /** Now checks if fetching a valid instruction from the
            configuration, if the end of the block has been
            reached and/or if a misspeculation ocurred. */
        if (indexCurrentBB < currCfg->mapped_blocks   &&
            currCfg->eip_start[indexCurrentBB] <= pc &&
            currCfg->eip_end  [indexCurrentBB] >= pc) {

            indexCurrentInst++;

            /** In case of non-speculative CPUs, this test is
                only an assertion; otherwise, it implies the
                fetch process must be stopped due to misspeculation. */
            if (!(currCfg->ordered_insts[indexCurrentInst]->pc == pc)) {
                DCACfgsExecuted[1]++;
                fetchingFromCfgCache = false;
            }

            inst->dcaInfo.levelInRA    =
                currCfg->ordered_insts[indexCurrentInst]
                ->completionLevel;

            /** Checks if should advance to next basic blk. */
            if (pc == currCfg->eip_end[indexCurrentBB]) {
                indexCurrentBB++;
            }

            DCAOpsExecuted++;
        } else if (indexCurrentBB == currCfg->mapped_blocks) {
            DCACfgsExecuted[0]++;
            fetchingFromCfgCache       = false;
        } else {
            DCACfgsExecuted[1]++;
            fetchingFromCfgCache       = false;
        }
    }
}

void
DCAO3CommitProbe::collectExecutionStats()
{
    if (cacheMissRA && memOrderViolation) {
        /* MOV & Cache miss */

        dcaCfgsExecuted[1]++;
        dcaCommittedOps[1] += bundledRAMicroops;
    } else if (cacheMissRA) {
        /* Cache miss only. */

        dcaCfgsExecuted[2]++;
        dcaCommittedOps[2] += bundledRAMicroops;
    } else if (memOrderViolation) {
        /* MOV only. */

        dcaCfgsExecuted[3]++;
        dcaCommittedOps[3] += bundledRAMicroops;
    } else {
        assert(!cacheMissRA && !memOrderViolation);

        /* No MOV, no Cache miss */
        /* This is the only scenario where execution
           is actually counted. */

        dcaCfgsExecuted[0]++;
        dcaCommittedOps[0] += bundledRAMicroops;

        dcaUopCountDist.sample(bundledRAMicroops);
        dcaCycleCountDist.sample(bundledRACycles);
        dcaBlockCountDist.sample(bundledRABBs);

        int diff = bundledRACycles - bundledSSCycles;
        dcaCyclesDiffDist.sample(diff);
        dcaCyclesDiffTotal += diff;

        dcaUOPCDist.sample((float) bundledRAMicroops / bundledRACycles);

        if (bundledSSCycles > 10*bundledRACycles)
            dcaWarningLikelyWrong += diff;
    }
}

Cycles
DCAO3CommitProbe::getDCacheLatency()
{
    MemObject &obj   = cpu->getDataPort().getSlavePort().owner;
    Cache *cache = dynamic_cast<Cache*>(&obj);

    assert(cache != 0);

    return cache->getDataLatency();
}

void
DCAO3CommitProbe::regStats()
{
    dcaUopCountDist
        .init(0, 256, 1)
        .name(cpu->cpuId() + "." + name() + ".uopCountDist")
        .desc("Dist: number of uops per configuration executed")
        .flags(Stats::pdf | Stats::nozero | Stats::nonan);


    dcaCycleCountDist
        .init(0, 128, 1)
        .name(cpu->cpuId() + "." + name() + ".cycleCountDist")
        .desc("Dist: number of cycles per configuration executed")
        .flags(Stats::pdf | Stats::nozero | Stats::nonan);

    dcaBlockCountDist
        .init(0, 10, 1)
        .name(cpu->cpuId() + "." + name() + ".blockCountDist")
        .desc("Dist: number of blocks per configuration executed")
        .flags(Stats::pdf | Stats::nozero | Stats::nonan);

    dcaUOPCDist
        .init(0, 10, 0.5)
        .name(cpu->cpuId() + "." + name() + ".uOPCDist")
        .desc("Dist: microops per cycle per configuration executed")
        .flags(Stats::pdf | Stats::nozero | Stats::nonan);

    dcaCyclesDiffDist
        .init(-64, 64, 1)
        .name(cpu->cpuId() + "." + name() + ".cyclesDiffDist")
        .desc("Dist: diff in cycle count per cfg executed "
              "(w.r.t baseline - "
              "v > 0 means baseline was faster)")
        .flags(Stats::pdf | Stats::nozero | Stats::nonan);

    dcaCyclesDiffTotal
        .name(cpu->cpuId() + "." + name() + ".cyclesDiff")
        .desc("Total diff in cycle count per cfg executed "
              "(w.r.t. baseline - "
              "v > 0  means baseline was faster)")
        .flags(Stats::nozero | Stats::nonan);

    dcaCfgsExecuted
        .init(4)
        .name(cpu->cpuId() + "." + name() + ".cfgsExecuted")
        .desc("Number of cfgs executed")
        .subname(0, "OK")
        .subdesc(0, "No cache miss NOR MOV occurred")
        .subname(1, "CacheMiss&MOV")
        .subdesc(1, "Cache miss AND MOV occurred")
        .subname(2, "CacheMiss")
        .subdesc(2, "Cache miss occurred")
        .subname(3, "MOV")
        .subdesc(3, "MOV (Memory Order Violation) occurred")
        .flags(Stats::nozero | Stats::nonan);

    dcaCommittedOps
        .init(4)
        .name(cpu->cpuId() + "." + name() + ".uopsCommitted")
        .desc("Number of uops DCA-executed")
        .subname(0, "FromCfg::OK")
        .subname(1, "FromCfg::CacheMiss&MOV")
        .subname(2, "FromCfg::CacheMiss")
        .subname(3, "FromCfg::MOV")
        .flags(Stats::nozero | Stats::nonan);

    dcaWarningLikelyWrong
        .name(cpu->cpuId() + "." + name() + "cyclesDiffWARNINGLikelyError")
        .desc("Fraction of " + name() + ".cyclesDiff"
              "that should likely be ignored")
        .flags(Stats::nozero | Stats::nonan);
}

void
DCAO3CommitProbe::resetCounters()
{
    bundledRAMicroops = 0;
    bundledRAMemWriteMicroops = 0;
    bundledRAMemReadMicroops = 0;
    bundledRACycles = 0;
    bundledRABBs = 0;
    bundledSSCycles = 0;
    cacheMissRA = false;
    memOrderViolation = false;
    storeTbl.clear();
}

DCAO3CommitProbe::DCAO3CommitProbe(BaseCPU *cpu) : cpu(cpu), status(Idle)
{
    regStats();
}

void
DCAO3CommitProbe::probe(DynInstPtr inst)
{
    Cycles curCycle = cpu->curCycle();

    if (status == Idle) {
        if (inst->dcaInfo.firstFromRA) {
            assert(inst->dcaInfo.raInst);

            /* This instruction stream comes from DCA module.
               Starts evaluating gains. */
            resetCounters();

            status = CommittingCGRA;
        } else if (!inst->dcaInfo.firstFromRA && inst->dcaInfo.raInst) {

            /* Misspeculation detected. The instructions that originated
               this (partial) instruction streams were flushed.
               Ignore the remaining instructions from DCA. */
            status = Flushing;

        } else {
            assert(!inst->dcaInfo.firstFromRA && !inst->dcaInfo.raInst);

            /* Do nothing; keep waiting. */
        }
    } else if (status == CommittingCGRA) {
        if (!inst->dcaInfo.raInst) {

            /* Terminates this execution stream. */
            collectExecutionStats();
            resetCounters();

            status = Idle;
        } else if (inst->dcaInfo.raInst & inst->dcaInfo.firstFromRA) {

            /* Previous instruction stream has ended. Collect
               execution stats and continue in this state. */

            collectExecutionStats();
            resetCounters();
        } else {
            assert(inst->dcaInfo.raInst && !inst->dcaInfo.firstFromRA);

            /* Do a bunch of stuff and continue in this state. */

            bundledRAMicroops++;

            /* If it is a memory operation, perform special actions to check
               for memory order violation (MOV). */
            if (inst->opClass() == MemWriteOp) {
                bundledRAMemWriteMicroops++;

                /* Updates storeTbl to register the CGRA level where the
                   last store operation (this one) ocurred. */
                auto found = storeTbl.find(inst->effAddr);
                if (found != storeTbl.end()) {
                    storeTbl.erase(found);
                }
                storeTbl.insert({inst->effAddr, inst->dcaInfo.levelInRA});

            } else if (inst->opClass() == MemReadOp) {

                bundledRAMemReadMicroops++;

                if (inst->memCyclesTook > getDCacheLatency()) {
                    cacheMissRA = true;
                }

                auto found = storeTbl.find(inst->effAddr);
                unsigned loadLevel = inst->dcaInfo.levelInRA;

                if (found != storeTbl.end()) {
                    unsigned storeLevel = found->second;

                    /* Test if this inst results in a
                       load-after-store hazard. */
                    memOrderViolation =
                        (loadLevel < storeLevel ? true : false);
                }
            }

            if (inst->isControl()) {
                bundledRABBs++;
            }

            bundledRACycles =
                std::max(
                    bundledRACycles,
                    (int) inst->dcaInfo.levelInRA + 1);

            if (curCycle != lastCycle) {
                unsigned diff    = curCycle - lastCycle;
                bundledSSCycles += diff;
            }

        }

    } else if (status == Flushing) {
        if (!inst->dcaInfo.raInst) {

            /* Done flushing. Return to idle state. */
            status = Idle;

        } else if (inst->dcaInfo.raInst && inst->dcaInfo.firstFromRA) {
            /* This instruction stream comes from DCA module.
               Starts evaluating gains. */
            resetCounters();

            status = CommittingCGRA;
        } else {
            assert(inst->dcaInfo.raInst && !inst->dcaInfo.firstFromRA);
        }
    }

    lastCycle = curCycle;
}
