// -*- c-file-style: "m5"; -*-

#pragma once

#include "arch/utility.hh"
#include "config/the_isa.hh"
#include "cpu/bt/ra_configuration.hh"
#include "mem/mem_object.hh"

// Forward declarations
class BTUnit;
class BaseCPU;
class CfgCache;

template<class T> class RefCountingPtr;
class BTDynInst;
typedef RefCountingPtr<BTDynInst> BTDynInstPtr;


/* CPU -> CfgCache interface */
class CPU2CfgCachePort;
class CfgCache2CPUPort;

/* CPU -> BT interface */
class BT2CPUPort;
class CPU2BTPort;

/* BT -> CfgCache interface */
class BT2CfgCachePort;
class CfgCache2BTPort;




/*****************************************************************************/
/*****************************************************************************/
/************************** CPU -> CfgCache interface ************************/
/*****************************************************************************/
/*****************************************************************************/

class CPU2CfgCachePort : public BaseMasterPort
{
    friend class CfgCache2CPUPort;

    BaseCPU              *owner;
    bool                 _bound;
    CfgCache2CPUPort *slavePort;

  public:
    CPU2CfgCachePort(const std::string& name, BaseCPU *owner);

    void bind(BaseSlavePort& slave_port);
    void unbind();
    bool isBound() {return _bound;}

    /** Looks up in the CfgCache for the Cfg. matching
        the address in PCState. Returns a struct containing that
        Cfg's information. In case no Cfg. was found, the struct
        is returned with the numBlocks field = 0. */
    RAConfigurationPtr lookup(CGRAPCState addr);
};

class CfgCache2CPUPort : public BaseSlavePort
{
    friend class CPU2CfgCachePort;

    CfgCache           *owner;
    bool               _bound;
    CPU2CfgCachePort *masterPort;

  public:
    CfgCache2CPUPort(const std::string& name, CfgCache *owner);
    void setBound(bool bound) {_bound = bound;}

    bool isBound() {return _bound;}

    /** TODO Implement a generic 'receive' method */
};








/*****************************/
/***** CPU -> BT interface ***/
/*****************************/

class CPU2BTPort : public BaseMasterPort
{
  friend class BT2CPUPort;

private:
  BaseCPU         *owner;
  bool        _bound;
  BT2CPUPort *slavePort;

public:
  CPU2BTPort(const std::string& name, BaseCPU *owner);

  void bind(BaseSlavePort& slave_port);
  void unbind();
  bool isBound() { return _bound; }

  bool sendInstruction(BTDynInstPtr &inst, int cpuId);
};

class BT2CPUPort : public BaseSlavePort
{
  friend class CPU2BTPort;

private:
  BTUnit           *owner;
  bool        _bound;
  CPU2BTPort *masterPort;

public:
  BT2CPUPort(const std::string& name, BTUnit *owner);
  void setBound(bool bound) { _bound = bound; }

  bool isBound() {return _bound;}

  bool receiveInstruction(BTDynInstPtr &inst, int cpuId);
};


/*****************************/
/** BT -> CfgCache interface */
/*****************************/

class BT2CfgCachePort : public BaseMasterPort
{
  friend class CfgCache2BTPort;

private:
  BTUnit              *owner;
  bool                _bound;
  CfgCache2BTPort *slavePort;

public:
  BT2CfgCachePort(const std::string& name, BTUnit *owner);
  bool saveConfiguration(RAConfigurationPtr cfg);

  void bind(BaseSlavePort& slave_port);
  void unbind();
  bool isBound() { return _bound; }

};

class CfgCache2BTPort : public BaseSlavePort
{
  friend class BT2CfgCachePort;

private:
  CfgCache             *owner;
  bool                 _bound;
  BT2CfgCachePort *masterPort;

public:
  CfgCache2BTPort(const std::string& name, CfgCache *owner);
  bool saveConfiguration(RAConfigurationPtr cfg);

  void setBound(bool bound) { _bound = bound; }

  bool isBound() {return _bound;}


};
