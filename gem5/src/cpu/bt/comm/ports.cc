// -*- c-file-style: "m5"; -*-

#include "cpu/bt/comm/ports.hh"

#include "cpu/base.hh"
#include "cpu/bt/bt.hh"
#include "cpu/bt/configuration_cache.hh"
#include "debug/BT.hh"

/*****************************************************************************/
/*****************************************************************************/
/************************** CPU -> CfgCache interface ************************/
/*****************************************************************************/
/*****************************************************************************/

CPU2CfgCachePort::CPU2CfgCachePort(const std::string& name, BaseCPU *owner) :
    BaseMasterPort(name, owner), owner(owner), _bound(false)
{
}

void
CPU2CfgCachePort::bind(BaseSlavePort& slave_port)
{
    // Binds this port to the BT and does the mirror process as well
    _baseSlavePort = &slave_port;
    slavePort      = static_cast<CfgCache2CPUPort*>(&slave_port);

    if (slavePort == NULL) {
        fatal("Unable to bind ports (CPU2CfgCachePort::bind()).\n");
    } else {
        slavePort->_baseMasterPort = this;
        slavePort->masterPort = this;
        slavePort->setBound(true);
        _bound = true;
    }
}

void
CPU2CfgCachePort::unbind()
{
    if (_baseSlavePort == NULL)
        panic("Attempting to unbind master port %s that is not connected\n",
              name());

    slavePort->_baseMasterPort = NULL;
    slavePort->masterPort = NULL;
    slavePort  = NULL;
    _baseSlavePort = NULL;
    _bound = false;
    slavePort->setBound(false);
}


RAConfigurationPtr
CPU2CfgCachePort::lookup(CGRAPCState pc)
{
    CfgCache *cache = slavePort->owner;
    if (cache->lookupConfiguration(pc)) {
        return cache->cachedLookup;
    } else {
        return RAConfigurationPtr();
    }
}

CfgCache2CPUPort::CfgCache2CPUPort(const std::string& name, CfgCache *owner) :
    BaseSlavePort(name, owner), owner(owner), _bound(false)
{
}













CPU2BTPort::CPU2BTPort(const std::string& name, BaseCPU *owner) :
    BaseMasterPort(name, owner), owner(owner), _bound(false)
{
}

void
CPU2BTPort::bind(BaseSlavePort& slave_port)
{
    // Binds this port to the BT and does the mirror process as well
    _baseSlavePort = &slave_port;
    slavePort      = static_cast<BT2CPUPort*>(&slave_port);

    if (slavePort == NULL) {
        fatal("Unable to bind ports (CPU2BTPort::bind()).\n");
    } else {
        slavePort->_baseMasterPort = this;
        slavePort->masterPort = this;
        slavePort->setBound(true);
        _bound = true;
    }
}

void
CPU2BTPort::unbind()
{
    if (_baseSlavePort == NULL)
        panic("Attempting to unbind master port %s that is not connected\n",
              name());

    slavePort->_baseMasterPort = NULL;
    slavePort->masterPort = NULL;
    slavePort  = NULL;
    _baseSlavePort = NULL;
    _bound = false;
    slavePort->setBound(false);
}

bool
CPU2BTPort::sendInstruction(BTDynInstPtr& inst, int cpuId)
{
    return slavePort->receiveInstruction(inst, cpuId);
}

BT2CPUPort::BT2CPUPort(const std::string& name, BTUnit *owner) :
    BaseSlavePort(name, owner), owner(owner), _bound(false)
{
}

bool
BT2CPUPort::receiveInstruction(BTDynInstPtr& inst, int cpuId)
{
    if (owner != nullptr)
        owner->newInst(inst, cpuId);

    return true;
}










BT2CfgCachePort::BT2CfgCachePort(const std::string& name, BTUnit *owner) :
    BaseMasterPort(name, owner), owner(owner), _bound(false)
{
}

void
BT2CfgCachePort::bind(BaseSlavePort& slave_port)
{
    // Binds this port to the BT and does the mirror process as well
    _baseSlavePort = &slave_port;
    slavePort      = dynamic_cast<CfgCache2BTPort*>(&slave_port);

    if (slavePort == NULL) {
        fatal("Unable to bind ports (BT2CfgCachePort::bind()).\n");
    } else {
        slavePort->_baseMasterPort = this;
        slavePort->masterPort = this;
        slavePort->setBound(true);
        _bound = true;
        DPRINTF(BT,"BT2CfgCachePort is bound");
    }
}

void
BT2CfgCachePort::unbind()
{
    if (_baseSlavePort == NULL)
        panic("Attempting to unbind master port %s that is not connected\n",
              name());

    slavePort->_baseMasterPort = NULL;
    slavePort->masterPort = NULL;
    slavePort  = NULL;
    _baseSlavePort = NULL;
    _bound = false;
    slavePort->setBound(false);
}

bool
BT2CfgCachePort::saveConfiguration(RAConfigurationPtr cfg)
{
    if (!_bound)
    {
        panic("Attempting to save configuration through unbound port\n",
              name());
    }

    return slavePort->saveConfiguration(cfg);
}

CfgCache2BTPort::CfgCache2BTPort(const std::string& name, CfgCache *owner) :
    BaseSlavePort(name, owner), owner(owner), _bound(false)
{
}

bool
CfgCache2BTPort::saveConfiguration(RAConfigurationPtr cfg)
{
    return owner->saveConfiguration(cfg);
}

