// -*- c-file-style: "m5"; -*-

#pragma once

#include <fstream>
#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "base/refcnt.hh"
#include "base/types.hh"
#include "cpu/bt/dyn_inst.hh"
#include "cpu/func_unit.hh"
#include "cpu/o3/comm.hh"
#include "cpu/op_class.hh"
#include "cpu/reg_class.hh"
#include "cpu/static_inst.hh"
#include "params/BTUnit.hh"

enum CGRAMemoryOrderPolicy {
    FullInOrder,
    InOrder,
    SimplePred,
    None
};

/** Declare this hash function for RegId so that STL knows how to
    generate an unordered map of RegId. */
namespace std {
template<>
struct hash<RegId>
{
    size_t operator () (const RegId& x) const
    {
        return std::hash<int>()(x.classValue() ^ x.index() ^ x.elemIndex());
    }
};}

/** Just a PCState supporting the comparison operators we need. */
class CGRAPCState : public TheISA::PCState
{
  public:
    CGRAPCState() : TheISA::PCState() { }
    CGRAPCState(Addr a) : TheISA::PCState(a) { }

    CGRAPCState& operator=(const TheISA::PCState& d)
    {
        TheISA::PCState::operator=(d);
        return *this;
    }

    bool
    operator == (const TheISA::PCState &opc) const
    {
        return this->pc() == opc.pc() && this->upc() == opc.upc();
    }

    bool
    operator != (const TheISA::PCState &opc) const
    {
        return !(*this == opc);
    }

    bool
    operator < (const TheISA::PCState &rhs)
    {
        return
            (this->pc() < rhs.pc() ||
             (this->pc() == rhs.pc() &&
              this->upc() < rhs.upc()));
    }

    bool
    operator > (const TheISA::PCState &rhs)
    {
        return
            (this->pc() > rhs.pc() ||
             (this->pc() == rhs.pc() &&
              this->upc() > rhs.upc()));
    }

    bool
    operator >= (const TheISA::PCState &rhs)
    {
        return !(*this < rhs);
    }

    bool
    operator <= (const TheISA::PCState &rhs)
    {
        return !(*this > rhs);
    }
};


class BTUnit;
class RAConfiguration : public RefCounted
{
  private:

    /** The BTUnit who generated this configuration.
        Needed for acessing the parameters. */
    class BTUnit &generator;

    /* Parameters. All point to generator's parameters. */
    std::vector<FUDesc*>               &row_fu;
    enum CGRAMemoryOrderPolicy &memOrderPolicy;
    unsigned                        &cycleSize;
    unsigned                         &maxInsts;
    unsigned                          &maxRows;
    unsigned                      &maxBranches;

  protected:

  public:
    // Forward declaration - needed by class RAInst
    class RARow;

    /** A StaticInst and a PC. Notice that this class contains both
     *  static and dynamic members. The static members are filled at the
     *  BT stage and the dynamic members when the configuration is loaded. */
    class RAInst {
      private:
        friend class RARow;

        RAConfiguration *parentCfg;
        RARow           *parentRow;

      public:
        friend class RAConfiguration;
        //friend class RAConfiguration::RARow;

        CGRAPCState pc;
        StaticInstPtr staticInst;
        BTDynInstPtr dynInst;

        unsigned index;
        unsigned completionRow;
        unsigned completionLevel;

      public:
        RAInst(RAConfiguration::RARow* row,
               StaticInstPtr inst, unsigned index);

        std::string getStr();
        unsigned getIndex() { return index; }

        CGRAPCState pcState() { return pc; }
    };

    class RARow {
      private:
        friend class RAConfiguration;
        friend class RAInst;

        /* Configuration holding this row. */
        RAConfiguration *parent;

        /* Instructions allocated */
        std::vector<RAInst> cols;

        /* Allocated instructions per FU type*/
        std::vector<unsigned> used_fu_cols_per_row;

      public:
        /* Index of this row within the configuration */
        unsigned index;

      public:
        RARow(RAConfiguration*, unsigned int);

        bool canAddInstr(BTDynInstPtr const &) const;

        /** Tries to add instruction to the row. Returns a pointer to
            the RAInst* if success, nullptr otherwise. */
        RAInst* addInstr(BTDynInstPtr const &, unsigned index);

        std::vector<RAInst> getCols() const { return cols; }
        unsigned int numColsUsed() const {
            unsigned int num_cols_used = 0;
            for (unsigned int count: used_fu_cols_per_row)
                num_cols_used += count;
            return num_cols_used; }

        unsigned int getOPLatency(const BTDynInstPtr &inst);
        void allocFU(const BTDynInstPtr &inst);
        void deallocFU(const StaticInstPtr &inst);
    };

    unsigned mapped_instructions;

    /** This is exactly what is says: the number of blocks that were mapped.
     *  Each block is an (pc_start, pc_end) pair.
     */
    unsigned mapped_blocks;

    unsigned mapped_alu_instructions;
    unsigned mapped_mult_instructions;
    unsigned mapped_load_instructions;
    unsigned mapped_store_instructions;

    unsigned cycles;

    bool dynLock;

  private:

    /* Checks if a RAW exists between inst2 and inst1
       (where inst2 executes after inst1) */
    static bool hasRegRAW(StaticInstPtr inst1, StaticInstPtr inst2);

    /* Cached values */
    typename std::vector<RARow>::iterator rowsIt;

    unsigned last_load_row;
    unsigned last_store_row;

    typedef class std::unordered_map<Addr,unsigned> StoreTbl;
    StoreTbl storeTbl;

  public:

    /** Public constructor. */
    RAConfiguration(BTUnit&);

    bool supported(const StaticInstPtr&);

    std::vector<CGRAPCState> eip_start;
    std::vector<CGRAPCState> eip_end;

    /** Ordered list of all instructions mapped. */
    std::vector<RAInst*> ordered_insts;
    /** Rows */
    std::vector<RARow> rows;

    std::unordered_map<RegId, RARow*> dep_table;

    /** Logical registers read by this cfg */
    std::unordered_set<RegId> staticLiveIns ;
    /** Logical registers written to by this cfg */
    std::unordered_set<RegId> staticLiveOuts;

    unsigned dynMisspecCount;
    unsigned useCounter;

    bool addInstr(BTDynInstPtr const &);

    /* Checks in all rows if it is feasible to allocate a new instructions
       instr.
     *
     * Returns true if possible, false otherwise.
     *
     * The row where it can be scheduled is cached in [....]
     */
    bool canAddInstr(BTDynInstPtr const &);

    void checkCorrectness();

    bool empty() const;

    CGRAPCState getPCState() const;
    float getILP() const;

    unsigned getInstructions() const;

    void printAsDot(std::ostream&) const;

    bool incMisspecCount() {
        if (++dynMisspecCount == 2) {
            return true;
        } else {
            return false;
        }
    }

    void undoLastMap();

    static OpDesc* findOpClass(FUDesc* fu, OpClass op);
};

typedef RefCountingPtr<RAConfiguration> RAConfigurationPtr;
