// -*- c-file-style: "m5"; -*-

#pragma once

#include <queue>

#include "base/statistics.hh"
#include "cpu/base.hh"
#include "cpu/bt/comm/ports.hh"
#include "cpu/bt/dyn_inst.hh"
#include "cpu/bt/ra_configuration.hh"
#include "mem/mem_object.hh"
#include "params/BTUnit.hh"

typedef typename RAConfiguration::RARow   RARow;
typedef typename RAConfiguration::RAInst RAInst;

class BTUnit : public MemObject
{
  private:
    friend class RAConfiguration;

    enum BTStatus {
        Idle,
        Working
    };
    std::vector<BTStatus> _status;

    /* Parameters */
    std::vector<FUDesc*> supportedOps;
    CGRAMemoryOrderPolicy memOrderPolicy;
    unsigned cycleSize;
    unsigned maxInsts;
    unsigned maxRows;
    unsigned maxBranches;
    unsigned numCores;
    /** Receives instructions from the CPU. */
    std::vector<BT2CPUPort*> cpuPorts;
    /** Stores incoming instructions from the CPU. */
    std::vector<std::queue<BTDynInstPtr>> instsQ;
    //std::queue<BTDynInstPtr> instsQ;

    /** Sends instructions to Cfg cache. */
    BT2CfgCachePort cachePort;

    /** The tick event used for scheduling ticks. */
    EventFunctionWrapper tickEvent;

    /** Configuration being filled with instructions coming
        from commitQueue. */
    std::vector<RAConfigurationPtr> configurations;

    /** Whether the next inst read should correspond to
        the beginning of a new basic block. */
    std::vector<bool> newBlock;
    std::vector<unsigned> microopCount;
    std::vector<unsigned> microopIndex;

    /** Counts microops that were processed at BT. These microops can be
        - Discarded, for being a NOP operation
        - Discarded, for not being the first microop in a sequence
        - Discarded, for being an unsupported operation
        - Correctly mapped to a configuration
    */
    Stats::Vector processedMicroops;

    /** Counts number of configurations which were 'created'.
        Must match total number of configurations terminated. */
    Stats::Scalar createdCfgs;

    /** Counts configurations that were processed and sent to the cache.
        These configurations can be
        - Completed after succesfully mapping all basic blocks
        - Completed after failing to map an unsupported microop
        - Completed after failing to map a microop (conf. full)
    */
    Stats::Vector completedCfgs;



  public:
    BTUnit(BTUnitParams *p);

    void beginTranslation(unsigned cpu_id);

    BaseMasterPort& getMasterPort(const std::string& if_name,
                                  PortID idx = InvalidPortID) override;

    BaseSlavePort&  getSlavePort(const std::string& if_name,
                                 PortID idx = InvalidPortID) override;

    void newInst(BTDynInstPtr &inst, unsigned cpu_id);

    void regStats() override;

    void setActiveThreads(std::list<ThreadID> *at_ptr);

    void undoCurrentMacroopMapping(unsigned cpu_id);
    void completeTranslation(unsigned cpu_id);

    void tick();
    bool abortCurrentTranslation(unsigned cpu_id);
};
