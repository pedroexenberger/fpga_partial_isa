//   -*- c-file-style: "m5"; -*-
/*
 * Copyright (c) 2018 The Embedded Systems Group, Institute of Informatics,
 *                    Federal University of Rio Grande do Sul (UFRGS).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: Marcelo Brandalero
 */

#pragma once

#include "cpu/base.hh"
#include "cpu/bt/comm/ports.hh"
#include "cpu/o3/impl.hh"
#include "cpu/op_class.hh"

/** Fetch stage probe. */
class DCAO3FetchProbe {
  /* Typedefs */
  public:
    typedef O3CPUImpl::DynInstPtr DynInstPtr;

  /* Fields */
  private:
    BaseCPU *cpu;

    /** If fetching a configuration from CfgCache. */
    bool fetchingFromCfgCache           = false;
    /** Info for the configuration being fetched. */
    RAConfigurationPtr currCfg;
    /** Index of the BB currently being fetched. */
    unsigned indexCurrentBB             = 0;
    /** Index of the instruction currently being fetched. */
    unsigned indexCurrentInst           = 0;

    /* Statistics */
    Stats::Vector DCACfgsExecuted;
    Stats::Scalar DCAOpsExecuted;

    void regStats();

  public:
    DCAO3FetchProbe(BaseCPU*);
    void probe(DynInstPtr);
};

/** Commit stage. */
class DCAO3CommitProbe {

  /* Typedefs */
  public:
    typedef O3CPUImpl::DynInstPtr DynInstPtr;

  /* Fields */
  private:
    BaseCPU *cpu;

    enum Status {
        Idle,
        CommittingCGRA,
        Flushing
    } status;

    /** If a cache miss occurred when executing the current
     * configuration. */
    bool cacheMissRA = false;
    /** If a mem order violation occurred when executing the
     * current configuration. */
    bool memOrderViolation = false;

    /** Number of instructions from the current RA stream */
    int bundledRAMicroops = 0;
    int bundledRAMemWriteMicroops = 0;
    int bundledRAMemReadMicroops = 0;

    /** Number of cycles from the current RA stream. Is equal to the
     *  highest row (level) that executed.
     */
    int bundledRACycles = 0;
    /** Number of cycles from the current stream, in case it were
     *  executed in the superscalar pipeline.
     */
    int bundledSSCycles = 0;
    /** Number of basic blocks in the current RA stream. */
    int bundledRABBs = 0;

    /** Holds the last cycle where a uop was committed. */
    Cycles lastCycle;

    std::unordered_map<Addr, unsigned> storeTbl;


    /* Statistics */
    Stats::Distribution dcaUopCountDist;
    Stats::Distribution dcaCycleCountDist;
    Stats::Distribution dcaBlockCountDist;
    Stats::Distribution dcaUOPCDist;

    /** Difference in (baseline cycles - DCA cycles). */
    Stats::Distribution dcaCyclesDiffDist;
    /** Total number of cycles difference */
    Stats::Scalar dcaCyclesDiffTotal;

    /** Number of configurations executed, sorted into
        - OK (no cache miss, nor MOV ocurred)
        - MOV & Cache Miss
        - Cache Miss
        - MOV */
    Stats::Vector dcaCfgsExecuted;
    /** Total number of instructions committed, sorted according to
        the classification above (for the configuration it originated):
        - OK (no cache miss, nor MOV ocurred)
        - MOV & Cache Miss
        - Cache Miss
        - MOV */
    Stats::Vector dcaCommittedOps;

    /** This counter holds a subset of dcaCyclesDiffTotal
        which are likely from a methodology error. */
    Stats::Scalar dcaWarningLikelyWrong;

  /* Methods */
  private:
    void collectExecutionStats();
    Cycles getDCacheLatency();
    std::string name() { return "DCACommitProbe"; }
    void regStats();
    void resetCounters();

  public:
    DCAO3CommitProbe(BaseCPU*);

    void probe(DynInstPtr);

};
