// -*- c-file-style: "m5"; -*-

#include "configuration_cache.hh"
#include "debug/BT.hh"

CfgCache::CfgCache(CfgCacheParams *p)
    : MemObject(p),

      //cpuPort(p->name + ".cpu_port", *this),
      btPort(p->name + ".bt_port", this),
      size(p->size),
      numCores(p->num_cores)
{

    for (int idx=0;idx<numCores;++idx){
        cpuPorts.push_back(new CfgCache2CPUPort(p->name + ".cpu_port", this));
    }
}

CfgCache::~CfgCache()
{
}

CfgCache*
CfgCacheParams::create()
{
    return new CfgCache(this);
}

void CfgCache::eraseCfg(RAConfigurationPtr cfg)
{
    CGRAPCState pc = cfg->getPCState();
    auto cfgIt = cfgMap.find(pc);
    assert((*cfgIt).second == cfg);
    cfgMap.erase(cfgIt);
}

bool CfgCache::saveConfiguration(RAConfigurationPtr cfg)
{
    CGRAPCState pcState = cfg->getPCState();

    if (cfgMap.find(pcState.pc()) != cfgMap.end()) {
        /** Configuration with this PC already exists in the cache.
            Do nothing. */
        DPRINTF(BT, "Configuration with PC 0x%x already in the cfg. cache. \
                     Nothing to do.\n.", pcState.pc());
        return false;
    } else {
        DPRINTF(BT, "Config. with PC 0x%x not yet in the cfg. cache. \
                     Inserting.\n",
                pcState.pc());

        if (cfgMap.size() == size) {
            /** Selects configuration for removal. */
            RAConfigurationPtr lruCfg = cfgPriorityQueue.top();
            auto lruCfgIt             = cfgMap.find(lruCfg->getPCState().pc());

            /** Removes LRU cfg. */
            cfgPriorityQueue.pop();
            cfgMap.erase(lruCfgIt);
        }

        /** Inserts new LRU cfg. */
        cfgMap.insert({pcState, cfg});
        cfgPriorityQueue.push(cfg);

        assert(cfgMap.size() <= size);
    }
    return true;
}

bool CfgCache::lookupConfiguration(CGRAPCState pc)
{
    ++cfgLookup;

    auto cfgIt = cfgMap.find(pc);
    if (cfgIt != cfgMap.end()) {
        ++cfgLookupHits;

        cachedLookup = (*cfgIt).second;
        cachedLookup->useCounter++;

        return true;
    } else {
        return false;
    }
}

RAConfigurationPtr CfgCache::lastLookup()
{
    return cachedLookup;
}

void CfgCache::regStats()
{
    MemObject::regStats();

    cfgLookup
        .name(name() + ".cfgLookups")
        .desc("Number of read accesses to the cache")
        .prereq(cfgLookup);
    cfgLookupHits
        .name(name() + ".LookupHits")
        .desc("Number of read accesses to the cache that resulted in a hit")
        .prereq(cfgLookupHits);
}

BaseMasterPort&
CfgCache::getMasterPort(const std::string& if_name, PortID idx)
{
    return MemObject::getMasterPort(if_name, idx);
}

BaseSlavePort&
CfgCache::getSlavePort(const std::string& if_name, PortID idx)
{
    if (if_name == "cpu_port" && idx < cpuPorts.size()) {
        // the slave port index translates directly to the vector position
        return *cpuPorts[idx];
    } else if (if_name == "bt_port") {
        return btPort;
    } else {
        return MemObject::getSlavePort(if_name, idx);

    }
}

