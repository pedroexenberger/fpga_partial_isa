from m5.params    import *
from m5.SimObject import SimObject

from MemObject    import MemObject

class CfgCache(MemObject):
    type       = "CfgCache"
    cxx_header = "cpu/bt/configuration_cache.hh"

    cpu_port   = VectorSlavePort(
        "Slave port connected to CPU. Handles read requests.")
    bt_port    = SlavePort(
        "Slave port connected to BT. Handles write requests.")

    # Parameters
    size = Param.Unsigned(256,
                          "Size of the configuration cache, in # entries.");

    num_cores     =     Param.Unsigned(1, "Number of processor cores")

    # Connects to slave ports
    def connectCPU(self, core):
      core.cfgcache_port = self.cpu_port
    def connectBT(self, bt):
      bt.cfgcache_port = self.bt_port
