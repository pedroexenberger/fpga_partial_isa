// -*- c-file-style: "m5"; -*-

#include <fstream>
#include <iomanip>

#include "base/output.hh"
#include "bt.hh"
#include "debug/BT.hh"

const unsigned PROCMOP_NOP = 0;
const unsigned PROCMOP_NOTFIRST = 1;
const unsigned PROCMOP_UNSUPPORTED = 2;
const unsigned PROCMOP_FULL = 3;
const unsigned PROCMOP_OK = 4;

const unsigned PROCCFG_ComplEmpty  = 0;
const unsigned PROCCFG_ComplUnsupp = 1;
const unsigned PROCCFG_ComplFull   = 2;
const unsigned PROCCFG_ComplOK     = 3;

const unsigned DISCCFG_UNSUPPORTED = 0;
const unsigned DISCCFG_FULL= 1;

void BTUnit::regStats()
{
    MemObject::regStats();

    processedMicroops
        .init(5)
        .name(name() + ".processedOps")
        .desc("Ops processed in " + name())
        .subname(PROCMOP_NOP, "NOP")
        .subname(PROCMOP_NOTFIRST, "NotFirst")
        .subname(PROCMOP_UNSUPPORTED, "Unsupported")
        .subname(PROCMOP_UNSUPPORTED, "Full")
        .subname(PROCMOP_OK, "OK");

    createdCfgs
        .name(name() + ".createdCfgs")
        .desc("Number of configurations created.");
    completedCfgs
        .init(4)
        .name(name() + ".completedCfgs")
        .desc("CFGs processed in " + name())
        .subname(PROCCFG_ComplEmpty, "DeletedDueToEmpty")
        .subname(PROCCFG_ComplUnsupp, "AbortedDueToUnsupported")
        .subname(PROCCFG_ComplFull, "AbortedDueToFull")
        .subname(PROCCFG_ComplOK, "CompleteOK");
}

BTUnit::BTUnit(BTUnitParams *p) :
    MemObject(p),
    _status(p->num_cores, Idle),

    /* Parameters */
    supportedOps(p->supported_ops),
    cycleSize(p->cycle_size),
    maxInsts(p->num_insts),
    maxRows(p->num_rows),
    maxBranches(p->num_branches),
    numCores(p->num_cores),
    //cpuPort(p->name + ".cpu_port", *this),
    //instsQ(),
    cachePort(p->name + ".cfgcache_port", this),

    tickEvent([this]{ tick(); }, "AtomicSimpleCPU tick",
              false, Event::CPU_Tick_Pri)
{

    std::string policy = p->mem_order_policy;

    if (policy == "FullInOrder") {
        memOrderPolicy = FullInOrder;
    } else if (policy == "InOrder") {
        memOrderPolicy = InOrder;
    } else if (policy == "SimplePrediction") {
        memOrderPolicy = SimplePred;
    } else if (policy == "None") {
        memOrderPolicy = None;
    } else {
        fatal("Invalid RA Memory order Policy. Options Are: {FullInOrder, \
                          InOrder, SimplePrediction, None}\n");
    }
    instsQ.resize(numCores);
    configurations.resize(numCores);
    //_status.resize(numCores);
    newBlock.resize(numCores);
    microopCount.resize(numCores);
    microopIndex.resize(numCores);

    for (int idx=0;idx<numCores;++idx){
        cpuPorts.push_back(new BT2CPUPort(p->name + ".cpu_port", this));
    }

}

void
BTUnit::setActiveThreads(std::list<ThreadID> *at_ptr)
{
    // activeThreads = at_ptr;
}

BTUnit*
BTUnitParams::create()
{
    return new BTUnit(this);
}

BaseMasterPort&
BTUnit::getMasterPort(const std::string& if_name, PortID idx)
{
    DPRINTF(BT,"Returning master port: %s", if_name);
    if (if_name == "cfgcache_port") {
        return cachePort;
    }else {
        return MemObject::getMasterPort(if_name, idx);
    }
}

BaseSlavePort&
BTUnit::getSlavePort(const std::string& if_name, PortID idx)
{
    if (if_name == "cpu_port" && idx < cpuPorts.size()) {
        // the slave port index translates directly to the vector position
        return *cpuPorts[idx];
    } else {
        return MemObject::getSlavePort(if_name, idx);
    }
}

void
BTUnit::newInst(BTDynInstPtr &inst, unsigned cpu_id)
{

  instsQ[cpu_id].push(inst);
  //instsQ.push(inst);

  if (!tickEvent.scheduled())
    schedule(tickEvent, nextCycle());
}

/** This function undos the translation of the current MACROop being
    translated.

    E.g.: say a MACROop consists of three microops, and the BT unit has
    already mapped two of these ops. Calling this function will undo these
    two mappings.
*/
void
BTUnit::undoCurrentMacroopMapping(unsigned cpu_id)
{
    unsigned insts_to_undo = microopIndex[cpu_id];
    while (insts_to_undo--) {
        configurations[cpu_id]->undoLastMap();
    }
}

/** Aborts current translation process if
    - inst is unsupported or
    - cfg is full.

    Returns true if configuration was sent to cache;
    else returns false. */
bool
BTUnit::abortCurrentTranslation(unsigned cpu_id)
{
    RAConfigurationPtr fillingCfg = configurations[cpu_id];
    undoCurrentMacroopMapping(cpu_id);

    unsigned numInsts = fillingCfg->mapped_instructions;
    unsigned numBlocks = fillingCfg->mapped_blocks;

    if (fillingCfg->empty()) {
        _status[cpu_id] = Idle;
        configurations[cpu_id] = NULL;
        completedCfgs[PROCCFG_ComplEmpty]++;

        return false;
    } else {
        fillingCfg->eip_end[numBlocks-1] =
            fillingCfg->ordered_insts[numInsts-1]->pcState();
        completeTranslation(cpu_id);
        return true;
    }
}

void
BTUnit::tick()
{
    for (unsigned cpu_id = 0; cpu_id < numCores; cpu_id++) {
        while (instsQ[cpu_id].size()) {
            RAConfigurationPtr fillingCfg;
            fillingCfg = configurations[cpu_id];
            /** Starts by reading from the IQ (note: no dequeue yet). */
            BTDynInstPtr &inst = instsQ[cpu_id].front();

            /** Optimization: If NOP inst, dequeue and proceed to next. */
            if (inst->staticInst()->isNop()) {
                instsQ[cpu_id].pop();
                processedMicroops[PROCMOP_NOP]++;
                continue;
            }

            if (_status[cpu_id] == Idle) {
                /** Check: if idle and not at the beginning of a
                    MACROop boundary, dequeue and proceed to next.
                */
#if THE_ISA != RISCV_ISA
                if (!inst->staticInst()->isFirstMicroop()) {
                    instsQ[cpu_id].pop();
                    processedMicroops[PROCMOP_NOTFIRST]++;
                    continue;
                }
#endif

                beginTranslation(cpu_id);

            } else {
                /* Check: if new MACROop, reset microop counter. */
                /* This needs to be done BEFORE checking for supported op,
                   because in case of unsupported the previous (incomplete)
                   microop mapping will be undone. */
                if (inst->staticInst()->isFirstMicroop() ||
                    !inst->staticInst()->isMicroop()) {
                    microopIndex[cpu_id] = 0;
                }

                /** Check: unsupported instruction.
                    In this case, this configuration is done, and
                    proceeds to next instruction.
                */
                if (!fillingCfg->supported(inst->staticInst())) {
                    processedMicroops[PROCMOP_UNSUPPORTED]++;

                     if (abortCurrentTranslation(cpu_id)) {
                        completedCfgs[PROCCFG_ComplUnsupp]++;
                    }

                     instsQ[cpu_id].pop();
                     continue;

                }
                /** Tries to map the instruction */
                bool success = fillingCfg->addInstr(inst);
                /** Check: if unsuccessful in mapping current inst,
                    undo/terminate. */
                if (!success) {
                    processedMicroops[PROCMOP_FULL]++;

                    if (abortCurrentTranslation(cpu_id)) {
                        completedCfgs[PROCCFG_ComplFull]++;
                    }

                    instsQ[cpu_id].pop();
                    continue;
                }

                ++microopCount[cpu_id];
                if (inst->staticInst()->isMicroop()){
                    ++microopIndex[cpu_id];
                }

                /* Updates initial cfg EIP if we just started this block */
                if (newBlock[cpu_id]) {

#if THE_ISA != RISCV_ISA
                    assert(inst->staticInst()->isFirstMicroop() ||
                           !inst->staticInst()->isMicroop());
#endif
                    fillingCfg->eip_start[fillingCfg->mapped_blocks++] =
                        inst->pcState();
                    assert(fillingCfg->mapped_blocks <= maxBranches);
                    newBlock[cpu_id] = false;
                }

                /* Updates final cfg EIP,
                 * if ctrl instr and inst is the last one.
                 * Test must be done to avoid microbranches. */
                if (inst->staticInst()->isControl()) {
#if THE_ISA != RISCV_ISA
                    assert(inst->staticInst()->isLastMicroop());
#endif

                    fillingCfg->eip_end[fillingCfg->mapped_blocks-1] =
                        inst->pcState();
                    if (fillingCfg->mapped_blocks == maxBranches) {
                        completeTranslation(cpu_id);
                        completedCfgs[PROCCFG_ComplOK]++;
                    } else {
                        newBlock[cpu_id] = true;
                    }
                }

                processedMicroops[PROCMOP_OK]++;
                instsQ[cpu_id].pop();
            }

        }
    }

}

void
BTUnit::beginTranslation(unsigned cpu_id)
{
    DPRINTF(BT, "Starting a new configuration. \n");
    if (!configurations[cpu_id]) {
        configurations[cpu_id] = new RAConfiguration(*this);
        createdCfgs++;
    }

    newBlock[cpu_id]     = true;
    microopCount[cpu_id] =    0;
    microopIndex[cpu_id] =    0;

    _status[cpu_id] = Working;
}


std::ofstream myfile;
std::stringstream ss;
static unsigned fileCounter = 0;


void

BTUnit::completeTranslation(unsigned cpu_id)
{
    RAConfigurationPtr fillingCfg;
    fillingCfg = configurations[cpu_id];
    fillingCfg->checkCorrectness();

    /** First instruction in configuration is either:
     *    a standalone instruction or
     *    is the first microop of a complex instruction
     */
    assert(!fillingCfg->ordered_insts.front()->staticInst->isMicroop() ||
           (fillingCfg->ordered_insts.front()->staticInst->isMicroop() &&
            fillingCfg->ordered_insts.front()->staticInst->isFirstMicroop()));

    /** Last instruction in configuration is either:
     *    a standalone instruction or
     *    is the last microop of a complex instruction
     */
    assert(!fillingCfg->ordered_insts.back()->staticInst->isMicroop() ||
           (fillingCfg->ordered_insts.back()->staticInst->isMicroop() &&
            fillingCfg->ordered_insts.back()->staticInst->isLastMicroop()));

    for (short i = 0; i < fillingCfg->mapped_blocks; i++) {
        assert(fillingCfg->eip_start[i].pc() != 0);
        assert(fillingCfg->eip_end  [i].pc() != 0);
    }

    /** Prints to a file. */
    if (fileCounter++ <= 2) {
        ss.str(std::string()); ss.clear();
        ss << simout.directory();
        ss << "/ra_cfg_" << std::setfill('0') << std::setw(3) <<
            fileCounter << ".dot";
        myfile.open(ss.str());
        if (!myfile.is_open()) {
            std::cout << "File " << ss.str() << " not open.\n";
        }
        fillingCfg->printAsDot(myfile);
        myfile.close();
    }

    if (fillingCfg->mapped_instructions > 1)
        cachePort.saveConfiguration(fillingCfg);

    configurations[cpu_id] = NULL;
    fillingCfg = NULL;

    _status[cpu_id] = Idle;
}

