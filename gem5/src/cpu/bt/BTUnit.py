from FuncUnit import *

from m5.params    import *
from m5.SimObject import SimObject

from MemObject    import MemObject

class BTUnit(MemObject):
    type       = "BTUnit"
    cxx_header = "cpu/bt/bt.hh"

    cfgcache_port = MasterPort(
        "Master port connected to CfgCache to send configurations.")
    cpu_port      = VectorSlavePort(
        "Slave ports connected to CPUs to \
        receive incoming instruction streams.")

    # Parameters

    num_cores     =     Param.Unsigned(1, "Number of processor cores")

    num_rows      =     Param.Unsigned(36,
                          "Number of CGRA basic operation rows.")
    cycle_size    =     Param.Unsigned(3,
                                "Number of CGRA basic operation rows" + \
                                "that fit in a single cycle.")
    supported_ops =     VectorParam.FUDesc("Ops supported, their" + \
                                       "corresponding latency and count " + \
                                       "(within a row).")
    num_branches  =     Param.Unsigned(3,
                                  "Number of branches supported in" + \
                                  "each configuration.")
    num_insts     =     Param.Unsigned(128,
                                  "Number of instructions supported" + \
                                  "in each configuration.")
    mem_order_policy =  Param.String("InOrder", "CGRA Memory Order Policy." + \
                                  "Options Are: {FullInOrder, InOrder," + \
                                  " SimplePrediction, None}")

    def connectCPU(self, cpu):
      #cpu.bt_port = self.btbus.slave
      cpu.bt_port = self.cpu_port
