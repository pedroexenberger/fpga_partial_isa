import os

def getStatsFiles():
    return [filename for filename in os.listdir('.') if filename.endswith('.txt')]

def getOutputFile(stats_file):
    cur_simulation = str(stats_file).replace(".txt", "").split('_')
    return cur_simulation[0] + cur_simulation[2] + ".out"

def getFinalTick(stats_file):
    with open(stats_file) as f:
            for line in f:
                if "final_tick" in line:
                    return line.split()[1]
    print("final tick not found in file {}".format(stats_file))

def appendFinalTick(final_tick, output_file):
    with open(output_file, "a+") as f:
        if os.path.getsize(output_file) > 0:
            lines = f.readlines()
            last_line = lines[len(lines) - 1]
            if hasFinalTick(last_line):
                print("file {0} already ends with final tick".format(output_file))
            else:
                f.write("-: -: {};E;0".format(final_tick))
        else:
            if (final_tick is not None):
                f.write("-: -: {};E;0".format(final_tick))

def hasFinalTick(last_line):
    return last_line.startswith("-: -:")

def main():
    stats_files = getStatsFiles()
    for stats_file in stats_files:
        final_tick = getFinalTick(stats_file)
        output_file = getOutputFile(stats_file)
        print("Appending final tick to file {}".format(output_file))
        appendFinalTick(final_tick, output_file)

if __name__ == "__main__":
    main()