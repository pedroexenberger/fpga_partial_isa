import argparse

bbs_dict = {}

def print_dist():
  print "BlockAddress,Count"
  for bb in bbs_dict:
    print str(bb) + "," + str(bbs_dict[bb])


def parse_line(line):
  # line format: " system.cpu 0x4132c9.2  :   JMP_I : wrip   , t1, t2  : "

  l   = line.split()
  pc  = int(l[1].split(".")[0], 0)

  if pc in bbs_dict:
    bbs_dict[pc] += 1
  else:
    bbs_dict[pc]  = 1

def main():
  parser = argparse.ArgumentParser(description='Process some integers.')
  parser.add_argument('--filename', type=str, required=True, \
                      help='Trace file name.')
  args = parser.parse_args()

  with open(args.filename) as f:
    while True:
      line = f.readline()
      if not line: break

      parse_line(line)

  print_dist()

if __name__ == "__main__":
  main()
