#include <stdio.h>
#include <stdlib.h>

int main() {
    printf("Environment var1, var2: %s, %s\n",
           getenv("MY_ENV_VAR1"), getenv("MY_ENV_VAR2"));

    return 0;
}
