# -*- coding: utf-8 -*-
import sys

MIN_TICKS = 140000*313 #no simulador é 160K ciclos * 313 fator de conversao ticks para ciclos

if len(sys.argv) < 2:
    print("Not enough arguments. Please specify the .out file to simplify.")
    exit()

in_file = sys.argv[1]
if "A15" in in_file:
    big_file = in_file
    little_file = big_file.replace("A15", "A7")
if "A7" in in_file:
    little_file = in_file
    big_file = little_file.replace("A7", "A15")

out_filename_big = big_file.replace(".out", "_simple.out")
out_file_big = open(out_filename_big, "w")
out_filename_little = little_file.replace(".out", "_simple.out")
out_file_little = open(out_filename_little, "w")

with open(big_file, "r") as fb:
    with open(little_file, "r") as fl:
        while True:
            line_big = fb.readline()
            if line_big.strip() == '':
                break
            data_big = line_big.split(":")
            data_big = data_big[2].split(";")
            out_file_big.write(line_big)

            line_little = fl.readline()
            if line_little.strip() == '':
                break
            data_little = line_little.split(":")
            data_little = data_little[2].split(";")
            out_file_little.write(line_little)

            if data_big[1] == "f" and data_little[1] == "f":
                curTick_big = data_big[0]
                curTick_little = data_little[0]
                while True:
                    skip_line_big = fb.readline()
                    skip_line_little = fl.readline()
                    if skip_line_big.strip() == '' and skip_line_little == '':
                        exit()
                    skip_data_big = skip_line_big.split(":")
                    skip_data_big = skip_data_big[2].split(";")
                    skip_data_little = skip_line_little.split(":")
                    skip_data_little = skip_data_little[2].split(";")
                    if skip_data_big[1] == "s" and skip_data_little[1] == "s":
                        if ((int(skip_data_big[0]) - int(curTick_big)) < MIN_TICKS) and ((int(skip_data_little[0]) - int(curTick_little)) < MIN_TICKS):
                            fb.readline()
                            fl.readline()
                            continue
                        else:
                            out_file_big.write(skip_line_big)
                            out_file_little.write(skip_line_little)
                            break
                    else:
                        out_file_big.write(skip_line_big)
                        out_file_little.write(skip_line_little)
                        break
