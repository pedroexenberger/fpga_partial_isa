#!/usr/bin/python
# -*- coding: utf-8 -*-

from Workload import Workload
from Block import BlockType

import collections

TICKS_PER_CYCLE = 313
#WORKLOAD_LOAD_COST = TICKS_PER_CYCLE*3000
WORKLOAD_LOAD_COST_BIG    = TICKS_PER_CYCLE*12000
WORKLOAD_LOAD_COST_LITTLE = TICKS_PER_CYCLE*17000
WORKLOAD_LOAD_COST_VID    = TICKS_PER_CYCLE*17000
WORKLOAD_LOAD_COST_NN    = TICKS_PER_CYCLE*17000
#MIN_CYCLES_RUNNING = TICKS_PER_CYCLE*40000
#MIN_CYCLES_RUNNING = TICKS_PER_CYCLE*80000
MIN_CYCLES_RUNNING = TICKS_PER_CYCLE*160000

PWR_LITTLE_FULL = 0.0531527
# PWR_LITTLE_NOFP = PWR_LITTLE_FULL*0.8
PWR_LITTLE_NOFP = 0.0466769
PWR_BIG_FULL = 1.0360443
#PWR_BIG_NOFP = PWR_BIG_FULL*0.66
PWR_BIG_NOFP = 0.587023

PWR_VIDEO_ACC = 0.02907273596
PWR_NN_ACC = 0.123938031

EMULATION_COST = 40

VIDEO_SPEEDUP = 75
NN_SPEEDUP	= 100

BIG_PROC = "A15"
LITTLE_PROC = "A7"

class Core(object):
	def __init__(self, workloads, core_desc, coreid):
		self.workloads       	= workloads #workloadQueue
		self.execTicks       	= 0
		self.overheadTicks   	= 0
		self.currWorkload    	= None
		self._hasFT			 	= core_desc["hasFT"]
		self._hasFP          	= core_desc["hasFP"]
		self._isBig			 	= core_desc["processor"] == BIG_PROC
		self._isLittle			= core_desc["processor"] == LITTLE_PROC #can also be an accelerator
		self._canEmulate		= core_desc["canEmulate"]
		self._isAcc				= core_desc["isAcc"]
		self._accType			= core_desc["accType"]
		self.coreid          	= coreid
		self.runningFor		 	= 0

		self.protectedTicks  	= 0
		self.unprotectedTicks 	= 0
		self.powerGatedTicks 	= 0

		self.usage_hist			= collections.OrderedDict()
		self.usage_hist[0]      = coreid*2+1
		self.migration_hist		= {}

	def getCurrTick(self):
		currTick = 0
		# if self.currWorkload is not None:
		# 	currTick = self.currWorkload.block_start
		return currTick + self.execTicks + self.overheadTicks

	def getBlockEndTick(self):
		return self.currWorkload.block_end

	def hasCurrWorkload(self):
		return self.currWorkload is not None	

	def getCurrWorkload(self):
		return self.currWorkload

	def getCoreID(self):
		return self.coreid	

	def getAccType(self):
		if self.isAcc():
			return self._accType
		else:
			return None

	def hasFP(self):
		return self._hasFP

	def isBig(self):
		return self._isBig

	def isLittle(self):
		return self._isLittle

	def isFaultTolerant(self):
		return self._hasFT

	def isAcc(self):
		return self._isAcc

	def hasWorkloads(self):
		return not self.workloads.isEmpty()

	def addWorkload(self, workload):
		core_support = "FP-ready" if self.hasFP() else "Integer-only" if not self.isAcc() else self.getAccType()
		core_type = "A15" if self.isBig() else "A7" if not self.isAcc() else "Accelerator"
		print("%s pushed into the queue of core id: %d %s %s at tick %d" % (workload.benchmark, self.coreid, core_support, core_type, self.getCurrTick()))
		self.workloads.putWorkload(workload)

	def addPowerGatedTicks(self, cycleCount):
		self.powerGatedTicks += cycleCount
		self.execTicks += cycleCount

	def getNextWorkload(self):
		return self.workloads.getWorkload()

	def getExecTicks(self):
		return self.execTicks

	def getProtectedTicks(self):
		return self.protectedTicks

	def getUnprotectedTicks(self):
		return self.unprotectedTicks

	def getPowerGatedTicks(self):
		return self.powerGatedTicks

	def nextWorkloadIsCritical(self):
		return self.workloads.seeHead().isCritical()

	def nextWorkloadIsWaitingFP(self):
		return self.workloads.seeHead().waitingFP

	def isIdle(self, currSystemLowerTick):
		#If the system lowest tick (tick being watched) is lower than the ticks
		#this core has executed, than this core is "still executing" and not idle
		# i.e., this core can be turned off while its execTicks is behind currentLowest tick
		# and it has no workload
		if self.execTicks <= currSystemLowerTick:
			#return not self.hasCurrWorkload() and not self.hasWorkloads() 
			return not self.hasCurrWorkload()
		else:
			return False
			

	def updateUsage(self, currSystemLowerTick):
		# use = 0 if self.isIdle(currSystemLowerTick) else 1
		# list_usage = list(self.usage_hist.items())
		# if list_usage[-1] == ((self.getCoreID()*2)+use):
		# 	self.usage_hist[currSystemLowerTick/TICKS_PER_CYCLE] = ((self.getCoreID()*2)+use)

		if self.isIdle(currSystemLowerTick):
			self.usage_hist[currSystemLowerTick/TICKS_PER_CYCLE] = self.getCoreID()*2
		else:
			self.usage_hist[currSystemLowerTick/TICKS_PER_CYCLE] = (self.getCoreID()*2)+1

	def getUsage(self):
		return self.usage_hist

	def getMigration(self):
		return self.migration_hist

	def SortUsage(self):
		orderedUsage = collections.OrderedDict(sorted(self.usage_hist.items()))
		self.usage_hist = orderedUsage

	def SortMigration(self):
		orderedMigration = collections.OrderedDict(sorted(self.migration_hist.items()))
		self.migration_hist = orderedMigration

	def CompactUsage(self):
		compact_dict = collections.OrderedDict()
		counter = 0
		prevUse = -1
		for use in self.usage_hist:
			if self.usage_hist[use] == prevUse:
				counter += 1
				if counter == 10000:
					compact_dict[use] = prevUse
					counter = 0
			else:
				compact_dict[use] = self.usage_hist[use]
				counter = 0
				prevUse = self.usage_hist[use]
		self.usage_hist = compact_dict

	def releaseWorkloadFromIntCore(self):
		"""
			Used by interger-only cores when an FP or Acc. operation is issued. 
			Execute the current integer block and return current workload for
			assignment on a FP-ready core. 
		"""
		if self.currWorkload.getIntBlockTicks(self.isBig()) < 0:
				print block_ticks
				exit(0)
		intBlockTicks = self.currWorkload.getIntBlockTicks(self.isBig())
		self.execTicks += intBlockTicks
		if self.currWorkload.isCritical() and self.isFaultTolerant():
			self.protectedTicks += intBlockTicks
		else:
			assert (not self.currWorkload.isCritical()), "Executing critical \
			application on non Fault Tolerant core"
			self.unprotectedTicks += intBlockTicks

		self.migration_hist[self.getCurrTick()/TICKS_PER_CYCLE] = self.coreid + 1

		if self.currWorkload.getBlockType() == BlockType.FP:
			self.currWorkload.waitingFP = True
		else:
			self.currWorkload.waitingFP = False
		return self.currWorkload

	def releaseWorkloadFromFPCore(self):
		"""
			Used by FP-ready cores to release its current workload for
			assignment on an integer-only core, or an accelerator.
		"""
		if self.currWorkload.getBlockType() != BlockType.FP:
			# se detém um bloco não-FP, executa o preambulo 'int' ate o bloco chegar e libera para o acelerador
			intBlockTicks = self.currWorkload.getIntBlockTicks(self.isBig())
			self.execTicks += intBlockTicks

		self.currWorkload.waitingFP = False
		self.migration_hist[self.getCurrTick()/TICKS_PER_CYCLE] = self.coreid + 1

		return self.currWorkload

	def releaseWorkloadFromAccCore(self):
		"""
			Used by Acc-ready cores to release its current workload for
			assignment on any gpp core.
		"""

		self.currWorkload.waitingFP = False
		self.migration_hist[self.getCurrTick()/TICKS_PER_CYCLE] = self.coreid + 1

		return self.currWorkload


	def loadNewWorkload(self):
		"""
			Used by both types of cores. Checks if there is a workload on
			the assignment queue and load it. If no work, return -1 for
			correct treatment.
		"""
		self.runningFor = 0
		if self.hasWorkloads():
			#models release and load workload cost
			
			self.currWorkload = self.workloads.getWorkload()
			# soma custos de migracao
			if self.isAcc():
				if self.getAccType() == BlockType.VIDEO:
					self.overheadTicks += WORKLOAD_LOAD_COST_VID
				if self.getAccType() == BlockType.NN:
					self.overheadTicks += WORKLOAD_LOAD_COST_NN
			elif self.isBig():
				self.overheadTicks += WORKLOAD_LOAD_COST_BIG
			else:
				self.overheadTicks += WORKLOAD_LOAD_COST_LITTLE

			# pega um bloco novo para dentro de si
			if self.loadNewBlock() != 0:
				#No new blocks on the workload. Finish loading the remaining
				#integer block and discard workload
				if not self.isAcc():
					self.finishWorkload()
					#self.loadNewWorkload()
					print("deu != 0")
			return 0
		else:
			self.currWorkload = None
			return -1

	def loadNewBlock(self):
		"""
			Used by both types of cores. If the core is FP-ready, execute
			the current FP block and load next block. If integer-only,
			simply load the next block (will be assigned to a FP-ready on next
			iteration) 
		"""
		if self.isAcc():
			#só vai receber workload do seu próprio tipo
			blockTicks = self.currWorkload.getBlockTicks(isBig=True) #pega dif de blocos referentes ao processador big
			if blockTicks > 0 :
				if self.getAccType() == BlockType.VIDEO:
					accBlockTicks = blockTicks/VIDEO_SPEEDUP
				elif self.getAccType() == BlockType.NN:
					accBlockTicks = blockTicks/NN_SPEEDUP
			# soma os ciclos que executou
			self.execTicks += accBlockTicks
			self.runningFor += accBlockTicks
			#print("acc block read %d - acc block ticks %d") % (blockTicks, accBlockTicks)
		elif self.hasFP() and self.currWorkload.getBlockType() == BlockType.FP:
			if self.currWorkload.getBlockTicks(self.isBig()) < 0:
				print block_ticks
				exit(0)
			fpBlockTicks = self.currWorkload.getBlockTicks(self.isBig())
			self.execTicks += fpBlockTicks
			if self.currWorkload.isCritical() and self.isFaultTolerant():
				self.protectedTicks += fpBlockTicks
			else:
				assert (not self.currWorkload.isCritical()),"Executing critical\
				application %s on non Fault Tolerant core %d" \
				% (self.currWorkload.benchmark,self.coreid)
				self.unprotectedTicks += fpBlockTicks

			self.runningFor += fpBlockTicks
			#print("fp block read %d") % (fpBlockTicks)
		return self.currWorkload.loadNextBlock()

	def keepWorkload(self):
		"""
			Used by FP-ready cores. This is only used when no other workloads
			are waiting on the queue. Execute the current integer block and load
			a new block (which executes the current FP block)
		"""
		
		if self.hasFP():
			if self.currWorkload.getIntBlockTicks(self.isBig()) < 0:
				print block_ticks
				exit(0)
			intBlockTicks = self.currWorkload.getIntBlockTicks(self.isBig())
			self.execTicks += intBlockTicks
			
			if self.currWorkload.isCritical() and self.isFaultTolerant():
				self.protectedTicks += intBlockTicks
			else:
				assert (not self.currWorkload.isCritical()),"Executing critical\
				application %s on non Fault Tolerant core %d" \
				% (self.currWorkload.benchmark,self.coreid)
				self.unprotectedTicks += intBlockTicks

			self.runningFor += intBlockTicks
			if self.loadNewBlock() == -1:
				#No new blocks on the workload. Finish loading the remaining
				#integer block and discard workload
				# Send to integer core to finish
				# self.finishWorkload()
				return -2
			return 0

	def emulateWorkload(self):
		"""
			Used by integer only cores. This is used when the integer core is
			allowed to emulate workloads. Executes the Int block, Emulates 
			the current FP block and load a new one 
		"""

		if self.currWorkload.getIntBlockTicks(self.isBig()) < 0:
				print block_ticks
				exit(0)
		intBlockTicks = self.currWorkload.getIntBlockTicks(self.isBig())
		self.execTicks += intBlockTicks
		if self.currWorkload.isCritical() and self.isFaultTolerant():
			self.protectedTicks += intBlockTicks
		else:
			assert (not self.currWorkload.isCritical()),"Executing critical\
			application %s on non Fault Tolerant core %d" \
			% (self.currWorkload.benchmark,self.coreid)
			self.unprotectedTicks += intBlockTicks

		self.runningFor += intBlockTicks

		if self.currWorkload.getBlockTicks(self.isBig()) < 0:
			print block_ticks
			exit(0)

		#Emulating has an extra cost. We model here with an average
		fpBlockTicks = self.currWorkload.getBlockTicks(self.isBig())*EMULATION_COST

		self.execTicks += fpBlockTicks
		if self.currWorkload.isCritical() and self.isFaultTolerant():
			self.protectedTicks += fpBlockTicks
		else:
			assert (not self.currWorkload.isCritical()),"Executing critical\
			application %s on non Fault Tolerant core %d" \
			% (self.currWorkload.benchmark,self.coreid)
			self.unprotectedTicks += fpBlockTicks

		self.runningFor += fpBlockTicks
		return self.currWorkload.loadNextBlock()

	def finishWorkload(self):
		#Last block is accounted as an integer block
		block_ticks = self.currWorkload.getIntBlockTicks(self.isBig())		
		if block_ticks < 0:
			print block_ticks
			exit(0)
		self.execTicks += block_ticks
		if self.currWorkload.isCritical() and self.isFaultTolerant():
			self.protectedTicks += block_ticks
		else:
			assert (not self.currWorkload.isCritical()),"Executing critical\
			application %s on non Fault Tolerant core %d" \
				% (self.currWorkload.benchmark,self.coreid)
			self.unprotectedTicks += block_ticks
		core_support = "FP-ready" if self.hasFP() else "Integer-only" if not self.isAcc() else self.getAccType()
		core_type = "A15" if self.isBig() else "A7" if not self.isAcc() else "Accelerator"
		print("Core %d - %s - %s finished workload %s at tick %d" % (self.getCoreID(),core_type, core_support, self.getCurrWorkload().benchmark, self.getCurrTick()))
		self.currWorkload = None

	def ExecutedMinCycles(self):
		# Minimum cycles apply to FP cores or Int cores that emulate FP
		if self._hasFP or (not self._hasFP and self._canEmulate):
			return self.runningFor >= MIN_CYCLES_RUNNING
		else:
			return True

	def getPower(self):
		if self.isAcc():
			if self.getAccType() == BlockType.VIDEO:
				return PWR_VIDEO_ACC
			elif self.getAccType() == BlockType.NN:
				return PWR_NN_ACC
		elif self.isBig():
				if self.hasFP():
					return PWR_BIG_FULL
				else:
					return PWR_BIG_NOFP
		else:
			if self.hasFP():
				return PWR_LITTLE_FULL
			else:
				return PWR_LITTLE_NOFP

	def ticksPerCycle(self):
		return TICKS_PER_CYCLE