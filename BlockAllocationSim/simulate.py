#!/usr/bin/python

from SystemManager import SystemManager
from Debug import bcol
import json
from pprint import pprint
import sys
import ptvsd
from random import shuffle


#cores_desc =  #False = Int; True = FP
# processors = ["A7","A7","A7","A7","A7","A7","A7","A7"]
# cores_desc = [  [True,True,True,True,True,True,True,True],
#   				[False,False,True,True,True,True,True,True],
#   				[False,False,False,False,True,True,True,True],
#   				[False,False,False,False,False,False,True,True]]
# processors = ["A15","A15","A15","A15"]
# cores_desc = [[True,True,True,True]]
# 			  [True,True,True,False],
# 			  [True,True,False,False],
# 			  [True,False,False,False]]
# processors = ["A7","A7"]
# cores_desc = [[True,True],
# 		  [True,False]]


# processors = ["A15","A15","A7","A7","A7","A7"]
# cores_desc = [[False,False,True,True,True,True]]
# cores_ft   = [[False,False,True,True,True,True]]

# benchmarks = ["basicmath","bitcount","blowfish-d","blowfish-e",
# "CRC32","dijkstra","FFT","FFT-i","gsm-d","gsm-e",
# "jpeg-d","jpeg-e","patricia","qsort","sha","stringsearch",
# "susan-c","susan-e","susan-s"]

# processors = ["A15","A15","A15","A15","A7","A7","A7","A7","A7","A7","A7","A7"]
# cores_desc = [[True,True,True,True,True,True,True,True,True,True,True,True]]


# benchmarks = ["basicmath","bitcount","blowfish-d","blowfish-e",
# "CRC32","dijkstra","FFT","FFT-i","gsm-d","gsm-e",
# "jpeg-d","jpeg-e","patricia","qsort","sha","stringsearch",
# "susan-c","susan-e","susan-s","basicmath","bitcount","blowfish-d","blowfish-e",
# "CRC32","dijkstra","FFT","FFT-i","gsm-d","gsm-e",
# "jpeg-d","jpeg-e","patricia","qsort","sha","stringsearch",
# "susan-c","susan-e","susan-s"]

NUM_RUNS = 1

def FPRatio(core_desc):
		num_fp = 0
		for core in core_desc:
			if core["core"]["hasFP"]:
				num_fp += 1

		return int((num_fp/float(len(core_desc)))*100)

def main():
	# vs code debug block 
 	#5678 is the default attach port in the VS Code debug configurations
	# sys.stderr.write("Waiting for debugger attach")
	# ptvsd.enable_attach(address=('localhost', 5678), redirect_output=True)
	# ptvsd.wait_for_attach()
	# breakpoint()
 	#end vs code debug block 
	with open(sys.argv[1],"r") as f:
		cores_desc = json.load(f)["cores"]
	with open(sys.argv[2],"r") as f:
		benchmarks = json.load(f)["workloads"]

	#generate 10 different orders for the benchmarks to execute
	benchmarks_orders = []
	for i in xrange(NUM_RUNS):
		#shuffle(benchmarks)
		benchmarks_orders.append(list(benchmarks))

	print "Cycles;Energy;Protected Cycles;Total Cycles;\
		Overhead;Protected Cycles;Unprotected Cycles;Power Gated Cycles"
	for run, benchmarks_o in enumerate(benchmarks_orders):		
		system = SystemManager(cores_desc,benchmarks_o,("hist_%d"%run))
		system.StartSystem()
		system.Run()


if __name__ == "__main__":	
	main()
