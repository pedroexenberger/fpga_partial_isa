#!/usr/bin/python
# -*- coding: utf-8 -*-

from Workload import Workload
from Core import Core
from WorkloadQueue import WorkloadQueue
from Debug import bcol
from Block import BlockType

import math
import sys

import numpy as np
from numpy import ma

#import matplotlib.pyplot as plt

# import plotly
# import plotly.graph_objs as go

DEBUG_BARRIER = 10

class SystemManager(object):
	def __init__(self, _cores_desc, _benchmarks, _histfile_name):

		self.cores_desc = _cores_desc
		self.benchmarks = _benchmarks
		self.cores = []
		self.isFullISAProcessor = True
		self.histfile_name = _histfile_name
		"""
			Responsible for managing workloads between cores
		"""

	def StartSystem(self):
		"""
			Loads workloads and cores to memory
		"""

		#bcol.I("Starting system parameters...")

		mainQueue = WorkloadQueue()
		for bench in self.benchmarks:
			#filenameBig = "%s%s.out" % (bench,"A15")
			#filenameLittle = "%s%s.out" % (bench,"A7")
			#workload = Workload(filenameBig,filenameLittle,bench)
			workload = Workload(bench["workload"])
			mainQueue.putWorkload(workload)

		#mainQueue.randomize()

		
		for key, core_cfg in enumerate(self.cores_desc):
			#workloadsPerCore = int(mainQueue.length()/len(self.cores_desc))
			
			# core = Core(mainQueue.distribute(
			# 		workloadsPerCore,
			# 		core_cfg["core"]["hasFT"],(not core_cfg["core"]["hasFP"])),
			# 		core_cfg["core"],key)
			# self.isFullISAProcessor = \
			# 	False if core_cfg["core"]["hasFP"] == False \
			# 						else self.isFullISAProcessor

			core = Core(WorkloadQueue(),
					core_cfg["core"],key)
			#self.isFullISAProcessor = \
			#	False if core_cfg["core"]["hasFP"] == False \
			#						else self.isFullISAProcessor
			self.cores.append(core)

		for core in self.cores:
			if core.getAccType() == BlockType.VIDEO:
				Workload.hasAccVideo = True
			elif core.getAccType() == BlockType.NN:
				Workload.hasAccNN = True
		

		self.Distribute(mainQueue)

		#Distribute the remaining worloads
		# while not mainQueue.isEmpty():
		# 	for core in self.cores:
		# 		if not mainQueue.isEmpty():
		# 			if mainQueue.seeHead().isForceInteger() and not core.hasFP():
		# 				if(mainQueue.seeHead().isCritical()):
		# 					if(core.isFaultTolerant()):
		# 						core.addWorkload(mainQueue.getWorkload())
		# 				else:
		# 					core.addWorkload(mainQueue.getWorkload())
		# 			#elif not mainQueue.seeHead().isForceInteger() or self.isFullISAProcessor:
		# 			elif not mainQueue.seeHead().isForceInteger():
		# 					if(mainQueue.seeHead().isCritical()):
		# 						if(core.isFaultTolerant()):
		# 							core.addWorkload(mainQueue.getWorkload())
		# 					else:
		# 						core.addWorkload(mainQueue.getWorkload())

		# assert mainQueue.isEmpty(), "Could not allocate all workloads! len %d" \
		# 												% len(mainQueue.queue)

	def Distribute(self, queue):
		while not queue.isEmpty():
			targetCoreFP = None
			targetCoreInt = None
			workload = queue.getWorkload()
			for core in self.cores:
				if not core.isAcc() and core.hasFP():
					if targetCoreFP is None:
						targetCoreFP = core
					else:
						if targetCoreFP.workloads.length() >= core.workloads.length(): 
							targetCoreFP = core
			for core in self.cores:
				if not core.isAcc() and not core.hasFP():
					if targetCoreInt is None:
						targetCoreInt = core
					else:
						if targetCoreInt.workloads.length() > core.workloads.length(): 
							targetCoreInt = core
			if targetCoreFP is None:
				targetCore = targetCoreInt
			elif targetCoreInt is None:
				targetCore = targetCoreFP
			elif targetCoreFP.workloads.length() == targetCoreInt.workloads.length():
				if workload.isForceInteger():
					targetCore = targetCoreInt
				else:
					targetCore = targetCoreFP
			elif targetCoreFP.workloads.length() > targetCoreInt:
				targetCore = targetCoreInt
			else:
				targetCore = targetCoreFP
			targetCore.addWorkload(workload)

		# intQueue = WorkloadQueue()
		# fPQueue = WorkloadQueue()

		# while not queue.isEmpty():
		# 	workload = queue.getWorkload()
		# 	if workload.isForceInteger():
		# 		intQueue.putWorkload(workload)
		# 	else:
		# 		fPQueue.putWorkload(workload)

		# while not intQueue.isEmpty():
		# 	targetCore = None
		# 	workload = intQueue.getWorkload()
		# 	for core in self.cores:
		# 		#neither FP cores nor accelerators receives integer workloads at first
		# 		if not core.hasFP() and not core.isAcc():
		# 			if targetCore is None:
		# 				targetCore = core
		# 			else:
		# 				if targetCore.workloads.length() > core.workloads.length(): 
		# 					targetCore = core
		# 	if targetCore is not None:
		# 		targetCore.addWorkload(workload)
		# 	else:
		# 		fPQueue.putWorkload(workload)

		# while not fPQueue.isEmpty():
		# 	targetCore = None
		# 	workload = fPQueue.getWorkload()
		# 	for core in self.cores:
		# 		if not core.isAcc():
		# 			if targetCore is None:
		# 				targetCore = core
		# 			else:
		# 				if targetCore.workloads.length() >= core.workloads.length(): 
		# 					targetCore = core
		# 	targetCore.addWorkload(workload)
		
		# for core in self.cores:
		# 		if not core.isAcc():
		# 			if targetCore is None:
		# 				targetCore = core
		# 			else:
		# 				if targetCore.workloads.length() >= core.workloads.length(): 
		# 					targetCore = core
		# 	targetCore.addWorkload(workload)

	def getLowerTickCore(self):
		"""
			Finds the core with lowest current tick execution
		"""
		currentLowerCore = None
		for core in self.cores:
			if core.hasWorkloads() or core.hasCurrWorkload():
				if currentLowerCore is None:
					currentLowerCore = core
				#If same tick, stick with current (priority is in array order)
				if currentLowerCore.getCurrTick() > core.getCurrTick():
					currentLowerCore = core
		# if currentLowerCore is None:
		# 	return self.EmptyWorkloads()

		return currentLowerCore

	def EmptyWorkloads(self):
		currentLowerCore = None
		for core in self.cores:
			if core.hasCurrWorkload():
				if currentLowerCore is None:
					currentLowerCore = core
				#If same tick, stick with current (priority is in array order)
				if currentLowerCore.getCurrTick() > core.getCurrTick():
					currentLowerCore = core

		return currentLowerCore

	def BalanceWorkloadGenerico(self, workload, activeCore):
		core_support = "FP-ready" if activeCore.hasFP() else "Integer-only" if not activeCore.isAcc() else activeCore.getAccType()
		core_type = "A15" if activeCore.isBig() else "A7" if not activeCore.isAcc() else "Accelerator"
		print ("core %d - %s - %s - released workload %s at tick %d" % (activeCore.getCoreID(), core_support, core_type, workload.benchmark, activeCore.getCurrTick()))
		self.syncIdleCores(activeCore)
		blockType = workload.getBlockType()
		if blockType != BlockType.FP and blockType != BlockType.INT:
			#if neither full nor partial core, is an acc
			self.BalanceAccWorkload(workload)
		else:
			if not activeCore.hasFP() and not activeCore.isAcc():
				self.BalanceFPWorkload(workload)
			else:
				self.BalanceWorkload(workload)

	def BalanceWorkload(self, workload):
		"""
			Finds the core with the smallest workload queue and add a new
			workload to it.
		"""
		
		#prioFPCore = False
		prioFPCore = not workload.forceIntegerCore
		criticalWorkload = workload.isCritical()
		targetCore = None
		for core in self.cores:
			if criticalWorkload:
				#Can only allocate to fault tolerant cores
				if core.isFaultTolerant():
					if targetCore is None:
						targetCore = core
					else:
						if targetCore.workloads.length() > \
													core.workloads.length():
							targetCore = core
						if prioFPCore and (targetCore.workloads.length() == \
													core.workloads.length()):
							targetCore = core
			else:
				#Any core is suitable
				if not core.isAcc():
					if targetCore is None:
							targetCore = core
					else:
						if targetCore.workloads.length() > core.workloads.length():
							targetCore = core
						if prioFPCore and (targetCore.workloads.length() == \
														core.workloads.length()):
								targetCore = core
		print ("balance")
		targetCore.addWorkload(workload)

	def BalanceIntWorkload(self, workload):
		"""
			Finds the core with the smallest workload queue and add a new
			workload to it.
		"""
		
		#prioFPCore = False
		prioFPCore = not workload.forceIntegerCore
		criticalWorkload = workload.isCritical()
		targetCore = None
		for core in self.cores:
			if criticalWorkload:
				#Can only allocate to fault tolerant cores
				if core.isFaultTolerant() and not core.hasFP():
					if targetCore is None:
						targetCore = core
					else:
						if targetCore.workloads.length() > \
													core.workloads.length():
							targetCore = core
						if prioFPCore and (targetCore.workloads.length() == \
													core.workloads.length()):
							targetCore = core
			else:
				if not core.hasFP() and not core.hasAcc():
					if targetCore is None:
							targetCore = core
					else:
						if targetCore.workloads.length() > core.workloads.length():
							targetCore = core
						if prioFPCore and (targetCore.workloads.length() == \
														core.workloads.length()):
								targetCore = core
		print ("balance int")
		targetCore.addWorkload(workload)

	def BalanceFPWorkload(self, workload):
		"""
			Finds the FP-ready core with the smallest workload queue and add a 
			new workload to it.
		"""
		criticalWorkload = workload.isCritical()
		targetCore = None
		for core in self.cores:
			if criticalWorkload:
				#Can only allocate to fault tolerant FP-ready cores
				if core.isFaultTolerant() and core.hasFP():
					if targetCore is None:
						targetCore = core
					else:
						if targetCore.workloads.length() > \
												core.workloads.length():
							targetCore = core
			else:
				#Any FP-ready core is suitable
				if core.hasFP() and not core.isAcc():
					if targetCore is None:
						targetCore = core
					else:
						if targetCore.workloads.length() > \
												core.workloads.length():
							targetCore = core
		print ("balance fp")
		targetCore.addWorkload(workload)

	def BalanceAccWorkload(self, workload):
		"""
			Finds the matching accelerator with the smallest workload queue and add a 
			new workload to it.
		"""
		targetCore = None
		for core in self.cores:
			if core.isAcc() and core.getAccType() == workload.getBlockType():
				if targetCore is None:
					targetCore = core
				else:
					if targetCore.workloads.length() > \
											core.workloads.length():
						targetCore = core
		targetCore.addWorkload(workload)

	def hasWorkloads(self):
		"""
			Checks if any core still has work to do
		"""
		for core in self.cores:
			if not core.workloads.isEmpty() or core.hasCurrWorkload():
				return True

		return False

	def getWorkloadFromBusiestInt(self):
		"""
			Finds the integer core with the largest workload queue and remove
			a workload from it.
		"""
		targetCore = None
		for core in self.cores:
			#if not core.hasFP():
				# Consider only integer workloads
			if core.hasWorkloads() and not core.nextWorkloadIsWaitingFP():
				if targetCore is None:
					targetCore = core
				elif targetCore.workloads.length() < core.workloads.length():
					targetCore = core

		if targetCore is not None and targetCore.hasWorkloads() and not targetCore.isAcc():
			workload = targetCore.getNextWorkload()
			print("Getting %s from the busiest int core %d" % (workload.benchmark, targetCore.getCoreID()))
			return workload
		else:
			return None

	def getWorkloadFromBusiestFP(self, isFaultTolerant):
		"""
			Finds the FP-ready core with the largest workload queue and remove
			a workload from it.
			Only returns critical workload if the processor has fault tolerance
		"""
		targetCore = None
		for core in self.cores:
			if core.hasFP():
				if core.hasWorkloads():
					if ((core.nextWorkloadIsCritical() \
						and isFaultTolerant) or \
					(core.nextWorkloadIsCritical()==False)):
						if targetCore is None:
							targetCore = core
						elif targetCore.workloads.length() < \
							core.workloads.length():
							targetCore = core
		if targetCore is not None and targetCore.hasWorkloads() and not targetCore.isAcc():
			workload = targetCore.getNextWorkload()
			print("Getting %s from the busiest FP core %d" % (workload.benchmark, targetCore.getCoreID()))
			return workload
		else:
			#return self.getWorkloadFromBusiestInt(isFaultTolerant)\
			return None

	def hasIdleBigFP(self):
		for core in self.cores:
			if core.isBig() and core.hasFP() and \
			not core.hasWorkloads() and not core.hasCurrWorkload():
				return True
		return False

	def hasIdleBigInt(self):
		for core in self.cores:
			if core.isBig() and not core.hasFP() and \
			not core.hasWorkloads and not core.hasCurrWorkload():
				return True
		return False

	def syncIdleCores(self, lowerTickCore):
		"""
			Idle cores which do not have workloads in its queue should be 
			treated as power gated during the time they stay idle.
			This function adds "power gated" cycles to the count of these
			processors for more accurate energy estimation
		"""
		if lowerTickCore is None:
			return
		for core in self.cores:
			if core.isIdle(sys.maxint):				
				powerGatedTicks = \
					lowerTickCore.getCurrTick() - core.getCurrTick()				
				#idle core is behind the lowest tick active core. Sync ticks
				if powerGatedTicks > 0:
					core.addPowerGatedTicks(powerGatedTicks)

	def updateCoresUsage(self, lowestTickCore):
		for core in self.cores:
			core.updateUsage(lowestTickCore.getCurrTick())


	def printstats(self):
		bcol.OK("Progress stats")
		for core in self.cores:
			core_support = "FP-ready" if core.hasFP() else "Integer-only" if not core.isAcc() else core.getAccType()
			core_type = "A15" if core.isBig() else "A7" if not core.isAcc() else "Accelerator"
			bcol.I("Core %d - %s - %s" % (core.getCoreID(),core_type, core_support))
			bcol.I("	queue size:\t%d" % core.workloads.length())
			if core.currWorkload is not None:
				bcol.I("	current workload:\t%s" % \
					core.currWorkload.benchmark)
				bcol.I("	integer workload:\t{}".format(core.currWorkload.forceIntegerCore)) #mudei aqui, nao fazia sentido ser %d
			else:
				bcol.I("	current workload:\tNone")
			bcol.I("	current tick count:\t%d" % core.getExecTicks())
			bcol.I("	current tick overhead:\t%d" % core.overheadTicks)
			bcol.I("	power gated ticks:\t%d" % core.powerGatedTicks)

	def printFinalStats(self):
		#bcol.I("Final stats")
		targetCore = self.cores[0]
		total_energy = 0.0
		total_cycles = 0
		total_cycles_ft = 0
		protected_energy = 0.0
		unprotected_energy = 0.0
		total_overhead = 0
		protected_cycles = 0
		unprotected_cycles = 0
		powerGated_cycles = 0
		total_migrations = 0
		for core in self.cores:
			protected_energy = (core.getProtectedTicks()/core.ticksPerCycle())*core.getPower()*2 
			unprotected_energy = ((core.getUnprotectedTicks()+core.overheadTicks)/core.ticksPerCycle())*core.getPower()
			#Does not count power gated cycles
			#total_energy += (protected_energy+unprotected_energy)
			total_energy += ((core.getExecTicks() + core.overheadTicks - core.getPowerGatedTicks()) / core.ticksPerCycle()) \
							* core.getPower()

			protected_cycles += core.getProtectedTicks()/core.ticksPerCycle()
			unprotected_cycles += (core.getUnprotectedTicks()+core.overheadTicks)/core.ticksPerCycle()
			powerGated_cycles += core.getPowerGatedTicks()/core.ticksPerCycle()

			curr = targetCore.getExecTicks() + targetCore.overheadTicks
			nxt = core.getExecTicks() + core.overheadTicks
			if curr < nxt:
				targetCore = core

			total_cycles += core.getExecTicks()
			total_overhead += core.overheadTicks

			total_migrations += len(core.migration_hist)

			if core.isFaultTolerant():
				total_cycles_ft += nxt

		#bcol.I("Final tick count:\t%d" % targetCore.getExecTicks())
		#bcol.I("Final tick overhead:\t%d" % targetCore.overheadTicks)
		# print "%d;%f;%d;%d;%d;%d;%d;%d" % ((targetCore.getExecTicks()+targetCore.overheadTicks)/core.ticksPerCycle(),total_energy,
		# 	total_cycles_ft,total_cycles,total_overhead,
		# 	protected_cycles,unprotected_cycles,powerGated_cycles)
		print "%d;%f;%d" % ((targetCore.getExecTicks()+targetCore.overheadTicks)/targetCore.ticksPerCycle(),total_energy,total_migrations)
	
	def CompactUsageData(self):
		for core in self.cores:
			core.SortUsage()
			core.CompactUsage()

			core.SortMigration()

	def GenerateCoreUsage(self):
		for core in self.cores:
			# last_tick = 1
			# value = 1	
			file = open(("%s_core%d.txt" % (self.histfile_name, core.getCoreID())),"w")
			for tick, new_value in core.getUsage().iteritems():
				file.write("%d;%d\n" % (tick,new_value))
				# while(last_tick < tick):
				# 	file.write("%d;%d\n" % (last_tick,value))
				# 	last_tick += 1
				# value = new_value
			file.close()


	def PlotUsage(self):
		f = plt.figure()
		for core in self.cores:
			plt.step(list(core.getUsage().keys()), 
				list(core.getUsage().values()), 
				label=('Core%d'%core.getCoreID()))

		plt.legend()
		plt.show()		
		f.savefig("fig_%s.pdf" % self.histfile_name, bbox_inches='tight')

	def PlotMigration(self):
		f = plt.figure()
		for core in self.cores:
			plt.plot(list(core.getMigration().keys()), 
				list(core.getMigration().values()), '.',
				label=('Core%d'%core.getCoreID()))

		plt.legend()
		plt.show()		
		f.savefig("fig_%s_migration.pdf" % self.histfile_name, bbox_inches='tight')



	# def PlotUsage(self):
		
	# 	plotly.offline.init_notebook_mode(connected=True)

	# 	trace = []
	# 	for core in self.cores:			
	# 		listKeys 	 = list(core.getUsage().keys())
	# 		listValues	 = list(core.getUsage().values())
	# 		trace.append(go.Scatter(
	# 		    x=listKeys,
	# 		    y=listValues,
	# 		    mode='lines+markers',
	# 		    name="'core%d'"%core.getCoreID(),
	# 		    hoverinfo='name',
	# 		    line=dict(
	# 		        shape='hv'
	# 		    )
	# 		))

	# 	layout = dict(
	# 	    legend=dict(
	# 	        y=0.5,
	# 	        traceorder='reversed',
	# 	        font=dict(
	# 	            size=16
	# 	        )
	# 	    )
	# 	)
	# 	fig = dict(data=trace, layout=layout)
	# 	plotly.offline.plot(fig, filename='line-shapes')


	def Run(self):

		hasWorkloads = True
		activeCore = None
		progress = DEBUG_BARRIER #check at start also

		#The big main loop
		while hasWorkloads:
			if progress == DEBUG_BARRIER:
				#self.printstats()
				#raw_input("Press Enter to continue...")
				progress = 0

			#previousActiveCore = activeCore if activeCore else None
			activeCore = self.getLowerTickCore()
			#activeCore.getCurrTick()
			if activeCore is None:
				hasWorkloads = False  
				continue

			#self.syncIdleCores(activeCore)
			#self.syncIdleCores(previousActiveCore)

			hasNewWorkload = 0

			if not activeCore.getCurrWorkload():
				hasNewWorkload = activeCore.loadNewWorkload()
			if activeCore.hasFP():
				#If active core is FP-ready and has other workloads on its queue
				#release core for newer workload. If queue is empty, execute
				#current workload
				if (activeCore.hasWorkloads() and activeCore.ExecutedMinCycles()) \
					or (activeCore.hasCurrWorkload() and activeCore.getCurrWorkload().getBlockType() != BlockType.FP):
					#quando tem workloads e precisa liberar a execução para outro core, seja porque ja rodou muito tempo, seja porque tem um bloco que nao sabe
					#resolver (não FP)
					print("acc")
					workload = activeCore.releaseWorkloadFromFPCore()
					self.BalanceWorkloadGenerico(workload, activeCore)
					#self.BalanceWorkload(workload)
					hasNewWorkload = activeCore.loadNewWorkload()
				elif (activeCore.hasCurrWorkload() and activeCore.isLittle() and self.hasIdleBigFP()):
					workload = activeCore.releaseWorkloadFromFPCore()
					print ("core singelinho (little) %d dando release no %s at tick %d" % (activeCore.getCoreID(), workload.benchmark, activeCore.getCurrTick()))
					self.BalanceWorkloadGenerico(workload, activeCore)
					#self.BalanceWorkload(workload)
					hasNewWorkload = activeCore.loadNewWorkload()
				else:			
					if activeCore.hasCurrWorkload():
						hasNewWorkload = activeCore.keepWorkload()
						# print("keep %s in core %d" % (activeCore.getCurrWorkload().benchmark, activeCore.getCoreID()))
						if(hasNewWorkload == -2):
							workload = activeCore.releaseWorkloadFromFPCore()
							self.BalanceWorkloadGenerico(workload, activeCore)
							hasNewWorkload = activeCore.loadNewWorkload()

				#Try to get a new workload from FP cores
				if hasNewWorkload == -1:
					workload = \
					self.getWorkloadFromBusiestFP(activeCore.isFaultTolerant())
					if workload is not None:
						activeCore.addWorkload(workload)
						activeCore.loadNewWorkload()
					else:
						workload = self.getWorkloadFromBusiestInt()
						if workload is not None:
							activeCore.addWorkload(workload)
							activeCore.loadNewWorkload()
			elif activeCore.isAcc():
				#testar tipo do acc
				if activeCore.hasCurrWorkload():
					workload = activeCore.releaseWorkloadFromAccCore()
					self.BalanceWorkloadGenerico(workload, activeCore)
					activeCore.loadNewWorkload() #isso aqui precisa ter porque é o que força botar null no currWorkload se nao tiver mais workloads
			else:
				#If active core is integer-only, 
				#either emulate or send workload to FP-ready core
				if activeCore.ExecutedMinCycles() and activeCore.hasCurrWorkload():
					#always true for integer core that does not emulate
					workload = activeCore.releaseWorkloadFromIntCore()
					self.BalanceWorkloadGenerico(workload, activeCore)
					#self.BalanceFPWorkload(workload)
					hasNewWorkload = activeCore.loadNewWorkload()
				else:
					if activeCore.hasCurrWorkload():
						hasNewWorkload = activeCore.emulateWorkload()
						if(hasNewWorkload == -2):
							workload = activeCore.releaseWorkloadFromIntCore()
							self.BalanceWorkloadGenerico(workload, activeCore)
							hasNewWorkload = activeCore.loadNewWorkload()

				#Try to get a new workload from to integer cores
				if hasNewWorkload == -1:
					workload = \
					self.getWorkloadFromBusiestInt()
					if workload is not None:
						activeCore.addWorkload(workload)
						activeCore.loadNewWorkload()


			hasWorkloads = self.hasWorkloads()
			
			# for core in self.cores:
			# 	if not core.hasCurrWorkload() and not core.hasWorkloads() and not core.isAcc():
			# 		if not core.hasFP():
			# 			workload = self.getWorkloadFromBusiestInt()
			# 			if workload is not None:
			# 				core.addWorkload(workload)
			# 				core.loadNewWorkload()
			# 		else:
			# 			workload = \
			# 			self.getWorkloadFromBusiestFP(core.isFaultTolerant())
			# 			if workload is not None:
			# 				core.addWorkload(workload)
			# 				core.loadNewWorkload()
			# 			else:
			# 				workload = self.getWorkloadFromBusiestInt()
			# 				if workload is not None:
			# 					core.addWorkload(workload)
			# 					core.loadNewWorkload()
			

			#activeCore.updateUsage()
			progress += 1

			newLowerTickCore = self.getLowerTickCore()
			if newLowerTickCore is not None:
				self.updateCoresUsage(newLowerTickCore)

		#self.printstats()
		self.printFinalStats()
		# self.CompactUsageData()
		# self.GenerateCoreUsage()
		# self.PlotUsage()
		# self.PlotMigration()
		# 	