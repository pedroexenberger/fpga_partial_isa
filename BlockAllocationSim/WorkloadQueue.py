#!/usr/bin/python

from collections import deque
from random import shuffle

class WorkloadQueue(object):
	def __init__(self):
		"""
			The Workload Queue is a FIFO used by cores to buffer workloads.
		"""
		self.queue = deque([])


	def getWorkload(self):
		"""
    		Return the first workload of the queue
    	"""
		if self.isEmpty():
			return None
		else:
			return self.queue.popleft()

	def putWorkload(self, workload):
		"""
			Add a new workload to the end of the queue
		"""
		self.queue.append(workload)

	def isEmpty(self):
		"""
			Checks if there is any workloads on the queue
		"""
		return len(self.queue)==0

	def length(self):
		return len(self.queue)

	def randomize(self):
		"""
			Shuffles the order of the workloads. Used only when initializing
			the system
		"""
		shuffle(self.queue)

	def seeHead(self):
		return self.queue[0]

	def distribute(self, count, isFT, isInt):
		newQueue = WorkloadQueue()
		i = 0
		while count>i:
			if len(self.queue) > 0:
				if self.seeHead().isForceInteger():
					if isInt:
						if self.seeHead().isCritical():
							if isFT:
								newQueue.putWorkload(self.getWorkload())
								i += 1
							else:
								#Try next workload
								workload = self.getWorkload()
								self.putWorkload(workload)
						else:
							newQueue.putWorkload(self.getWorkload())
							i += 1
					else:
						if self.seeHead().isCritical():
							if isFT:
								newQueue.putWorkload(self.getWorkload())
								i += 1
							else:
								#Try next workload
								workload = self.getWorkload()
								self.putWorkload(workload)
						else:
							newQueue.putWorkload(self.getWorkload())
							i += 1
				else:
					if not isInt:
						if self.seeHead().isCritical():
							if isFT:
								newQueue.putWorkload(self.getWorkload())
								i += 1
							else:
								#Try next workload
								workload = self.getWorkload()
								self.putWorkload(workload)
						else:
							newQueue.putWorkload(self.getWorkload())
							i += 1
					else:
						if self.seeHead().isCritical():
							if isFT:
								newQueue.putWorkload(self.getWorkload())
								i += 1
							else:
								#Try next workload
								workload = self.getWorkload()
								self.putWorkload(workload)
						else:
							newQueue.putWorkload(self.getWorkload())
							i += 1
			else:
				break
		return newQueue