#!/usr/bin/python

base_dir = "Results/ESWEEK/"

configs = {"A15_4F0P",
			"A15_3F1P",
			"A15_2F2P",
			"A15_1F3P"}

workloads = {"scenario1",
			 "scenario2",
		     "scenario3",
		     "scenario4",
		  	 "scenario5",
			 "scenario6"}

def main():
	i=0
	for config in configs:
		for workload in workloads:
			f = open(base_dir+workload+"_results_"+config,"r")
			f.readline()
			line = f.readline()
			line = line.split(";");
			cycles[i] = line[0]
			energy[i] = line[1]
			close(f)
			i+=1

	


if __name__ == "__main__":	
	main()