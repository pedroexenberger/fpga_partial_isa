#!/usr/bin/python

base_dir = "Results/ESWEEK19/"

# configs = ["A15_4F0P","A15_3F1P","A15_2F2P","A15_1F3P"]
# configs = ["A15_1F0P", "A15_0F1P_A7_2F0P" ,"A15_0F1P_A7_1F1P"]
# configs = ["A15_1F0P_A7_2F0P", "A15_0F1P_A7_2F0P" ,"A15_0F1P_A7_1F1P"]
# configs = ["A15_1F0P_A7_2F0P", "A15_0F1E_A7_2F0P" ,"A15_0F1E_A7_1F1E"]
configs = ["A15_1F0P_A7_2F0P", "A15_0F1P_A7_4F0P" ,"A15_0F1E_A7_2F2E"]
 
workloads = ["scenario1", "scenario2", "scenario3","scenario4","scenario5","scenario6"]

def main():
	
	cycles = []
	energy = []
		
	for workload in workloads:	
		for config in configs:
		
			#print workload+"_results_"+config+".txt"
			f = open(base_dir+workload+"_results_"+config+".txt","r")
			#f.readline()
			line = f.readlines()
			line = line[-1].split(";");
			cycles.append(line[0])
			energy.append(line[1])
			f.close()
			

	sep = ";"
	print sep.join(cycles)
	print sep.join(energy)

if __name__ == "__main__":	
	main()
