#!/bin/bash


CONFIG_DIR="Configs/PEDRO"
WORKLOAD_DIR="Workloads/PEDRO"
RESULT_DIR="Results/PEDRO"
RESULT_FILENAME="_results_"

# declare -a arr_configs=("A15_1F0P.json"
#   						"A15_0F1P_A7_2F0P.json")

# declare -a arr_configs=("A15_1F0P.json"
#    						"A15_0F1P_A7_2F0P.json"
#    						"A15_0F1P_A7_1F1P.json"
#    						"A15_2F0P.json"
#    						"A15_1F1P.json"
# 						"A15_4F0P.json"
#  						"A15_3F1P.json"
#  						"A15_2F2P.json"
#  						"A15_1F3P.json"
# 						"A15_0F1E_A7_1F1E.json"
#    						"A15_0F1E_A7_2F0P.json"
#    						"A15_1F0P_A7_2F0P.json"
#    						"A15_0F1P_A7_4F0P.json"
#  						"A15_0F1E_A7_4F0P.json")

 declare -a arr_configs=("baseline.json" "task_parallel.json" "acc_video.json" "acc_nn.json")


 declare -a workloads=("scenario_task_parallel.json" "scenario_task_parallel2.json" "scenario_task_parallel3.json")
#declare -a workloads=("scenario1.json"
#					  "scenario2.json"
#					  "scenario3.json"
#					  "scenario4.json"
#					  "scenario5.json"
#					  "scenario6.json")

# declare -a arr_configs=("A15_2F0P.json")


# declare -a workloads=("scenario2.json" "scenario3.json")



for config in "${arr_configs[@]}" 
do
	for workload in "${workloads[@]}"
	do
		echo "Running configuration "$config", scenario "$workload"."
 		python simulate.py $CONFIG_DIR/$config $WORKLOAD_DIR/$workload > $RESULT_DIR/${workload//".json"/}$RESULT_FILENAME${config//".json"/}".txt"
		#python simulate.py $CONFIG_DIR/$config $WORKLOAD_DIR/$workload
	done
done