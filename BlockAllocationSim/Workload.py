#!/usr/bin/python

from Debug import bcol
from Block import Block
from Block import BlockType

class Workload(object):
	hasAccVideo = False
	hasAccNN	= False
	def __init__(self, workload):
		"""
			The workload is a representation of an FP block from a given file
			of many blocks.
		"""
		#Could eventually create a class for the block, but I see no need for it
		#right now.
		self.fileBig = open("/mnt/BlockAllocationSim/%s" % \
			workload["file_big"],"r")
		self.fileLittle = open("/mnt/BlockAllocationSim/%s" % \
			workload["file_little"],"r")
		self.benchmark = workload["benchmark"]
		self._isCritical = workload["critical"]
		self.forceIntegerCore = workload["integer"]
		self.bigBlock = Block()
		self.littleBlock = Block()
		self.waitingFP = False

	def getBlockType(self):
		return self.bigBlock.getBlockType()

	def loadNextBlock(self):
		"""
			Parse and load a new line from the workload.
		"""

		lineBig = ''

		if not self.fileBig.closed:
			self.bigBlock.last_block_end = self.bigBlock.block_end
			self.littleBlock.last_block_end = self.littleBlock.block_end
			lineBig = self.fileBig.readline()
		if lineBig.strip() != '':
			dataset = lineBig.split(":")
			dataset = dataset[2].split(";")
			if dataset[1] == 's':
				self.bigBlock.block_start = int(dataset[0])
				self.bigBlock.block_id = int(dataset[2])
				lineBig = self.fileBig.readline()
				if lineBig.strip() != '':
					dataset = lineBig.split(":")
					dataset = dataset[2].split(";")
					if dataset[1] == 'f':
						self.bigBlock.block_end = int(dataset[0])
						#if found matching fp block 's','f' it is a fp block
						self.bigBlock.block_type = BlockType.FP
					else:
						bcol.E("ERROR: incompatible status %s on tick %d, \
						 	block %d, benchmark %s" % (status,
						 		curr_tick,block,benchmark))
						exit(0)
				else:
					bcol.E("ERROR: missing finish status on tick %d, block %d, \
					 	benchmark %s" % (status,curr_tick,block,benchmark))
					exit(0)
			elif dataset[1] == 'v':
				if Workload.hasAccVideo:
					self.bigBlock.block_start = int(dataset[0])
					self.bigBlock.block_id = int(dataset[2])
					while True:
						lineBig = self.fileBig.readline()
						if lineBig.strip() != '':
							dataset = lineBig.split(":")
							dataset = dataset[2].split(";")
							if dataset[1] == 'y':
								self.bigBlock.block_end = int(dataset[0])
								#if found matching fp block 's','f' it is a fp block
								self.bigBlock.block_type = BlockType.VIDEO
								break
						else:
							bcol.E("ERROR: missing finish status on tick %d, block %d, \
							 	benchmark %s" % (status,curr_tick,block,benchmark))
							exit(0)
				#else:
				#	return self.loadNextBlock() # le o proximo
			elif dataset[1] == 'y':
				pass
				#if not Workload.hasAccVideo:
				#	return self.loadNextBlock() # le o proximo
				#else:
				#	bcol.E("ERROR: found finish video status on tick %d, block %d, \
				#			 	benchmark %s" % (status,curr_tick,block,benchmark))
				#			exit(0)
			elif dataset[1] == 'n':
				if Workload.hasAccNN:
					self.bigBlock.block_start = int(dataset[0])
					self.bigBlock.block_id = int(dataset[2])
					while True:
						lineBig = self.fileBig.readline()
						if lineBig.strip() != '':
							dataset = lineBig.split(":")
							dataset = dataset[2].split(";")
							if dataset[1] == 'm':
								self.bigBlock.block_end = int(dataset[0])
								#if found matching fp block 's','f' it is a fp block
								self.bigBlock.block_type = BlockType.NN
								break
						else:
							bcol.E("ERROR: missing finish status on tick %d, block %d, \
							 	benchmark %s" % (status,curr_tick,block,benchmark))
							exit(0)
				#else:
				#	return self.loadNextBlock() # le o proximo
			elif dataset[1] == 'm':
				pass
				# if not Workload.hasAccNN:
				# 	return self.loadNextBlock() # le o proximo
				# else:
				# 	bcol.E("ERROR: found finish video status on tick %d, block %d, \
				# 			 	benchmark %s" % (status,curr_tick,block,benchmark))
				# 			exit(0)
			elif dataset[1] == 'E':
				#Load (E)xist block - it is accounted as an integer block
				self.bigBlock.block_start = int(dataset[0])
				self.bigBlock.block_id = int(dataset[2])
				self.bigBlock.block_end = int(dataset[0])
				self.bigBlock.block_type = BlockType.INT
				#bcol.OK("Done executing benchmark %s" % self.benchmark)
				self.fileBig.close()
			else:
				bcol.E("ERROR: incompatible status %s on tick %d, block %d, \
				 	benchmark %s" % (status,curr_tick,block,benchmark))
				exit(0)
		#else:
			#bcol.OK("Done executing benchmark %s" % self.benchmark)
			#self.fileBig.close()


		lineLittle = '' if self.fileLittle.closed else self.fileLittle.readline()
		if lineLittle.strip() != '':
			dataset = lineLittle.split(":")
			dataset = dataset[2].split(";")

			if dataset[1] == 's':
				self.littleBlock.block_start = int(dataset[0])
				self.littleBlock.block_id = int(dataset[2])
				lineLittle = self.fileLittle.readline()
				if lineLittle.strip() != '':
					dataset = lineLittle.split(":")
					dataset = dataset[2].split(";")
					if dataset[1] == 'f':
						self.littleBlock.block_end = int(dataset[0])
						# if found matching s-f block, we just read an fp block
						self.littleBlock.block_type = BlockType.FP
						return 0
					else:
						bcol.E("ERROR: incompatible status %s on tick %d, \
							block %d, benchmark %s" % (status,
								curr_tick,block,benchmark))
						exit(0)
				else:
					bcol.E("ERROR: missing finish status on tick %d, block %d, \
					 	benchmark %s" % (status,curr_tick,block,benchmark))
					exit(0)
			elif dataset[1] == 'v':
				if Workload.hasAccVideo:
					self.littleBlock.block_start = int(dataset[0])
					self.littleBlock.block_id = int(dataset[2])
					while True:
						lineLittle = self.fileLittle.readline()
						if lineLittle.strip() != '':
							dataset = lineLittle.split(":")
							dataset = dataset[2].split(";")
							if dataset[1] == 'y':
								self.littleBlock.block_end = int(dataset[0])
								#if found matching fp block 's','f' it is a fp block
								self.littleBlock.block_type = BlockType.VIDEO
								return 0
						else:
							bcol.E("ERROR: missing finish status on tick %d, block %d, \
							 	benchmark %s" % (status,curr_tick,block,benchmark))
							exit(0)
				else:
					return self.loadNextBlock() # le o proximo
			elif dataset[1] == 'y':
				if not Workload.hasAccVideo:
					return self.loadNextBlock() # le o proximo
				else:
					bcol.E("ERROR: found finish video status on tick %d, block %d, \
							 	benchmark %s" % (status,curr_tick,block,benchmark))
					exit(0)
			elif dataset[1] == 'n':
				if Workload.hasAccNN:
					self.littleBlock.block_start = int(dataset[0])
					self.littleBlock.block_id = int(dataset[2])
					while True:
						lineLittle = self.fileLittle.readline()
						if lineLittle.strip() != '':
							dataset = lineLittle.split(":")
							dataset = dataset[2].split(";")
							if dataset[1] == 'm':
								self.littleBlock.block_end = int(dataset[0])
								#if found matching fp block 's','f' it is a fp block
								self.littleBlock.block_type = BlockType.NN
								return 0
						else:
							bcol.E("ERROR: missing finish status on tick %d, block %d, \
							 	benchmark %s" % (status,curr_tick,block,benchmark))
							exit(0)
				else:
					return self.loadNextBlock() # le o proximo
			elif dataset[1] == 'm':
				if not Workload.hasAccNN:
					return self.loadNextBlock() # le o proximo
				else:
					bcol.E("ERROR: found finish NN status on tick %d, block %d, \
							 	benchmark %s" % (status,curr_tick,block,benchmark))
					exit(0)
			elif dataset[1] == 'E':
				#Load (E)xit block - it is accounted as an integer block
				self.littleBlock.block_start = int(dataset[0])
				self.littleBlock.block_id = int(dataset[2])
				self.littleBlock.block_end = int(dataset[0])
				self.littleBlock.block_type = BlockType.INT
				#bcol.OK("Done executing benchmark %s" % self.benchmark)
				self.fileLittle.close()
				return -1
			else:
				bcol.E("ERROR: incompatible status %s on tick %d, block %d, \
				 	benchmark %s" % (status,curr_tick,block,benchmark))
				exit(0)
		else:
			#self.fileLittle.close()
			return -1
			

	def getIntBlockTicks(self, isBig):
		"""
			Return the ticks from the current integer block. The integer block
			is given by the ticks between the current block start and the
			last block end.
		"""
		if isBig:
			return (self.bigBlock.block_start - self.bigBlock.last_block_end)
		else:
			return (self.littleBlock.block_start - self.littleBlock.last_block_end)

	def getBlockTicks(self, isBig):
		"""
			Return the ticks from the current FP block. The FP block
			is given by the ticks between the current block end and the
			current block start.
		"""
		if isBig:
			return (self.bigBlock.block_end - self.bigBlock.block_start)
		else:
			return (self.littleBlock.block_end - self.littleBlock.block_start)

	def isCritical(self):
		return self._isCritical

	def isForceInteger(self):
		return self.forceIntegerCore