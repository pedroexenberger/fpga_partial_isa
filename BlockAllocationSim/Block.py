#!/usr/bin/python

# using as enum
class BlockType:
	INT = "int"
	FP = "fp"
	VIDEO = "video"
	NN = "nn"
	NONE = None


class Block(object):
	def __init__(self):

		self.block_start = 0
		self.block_end = 0
		self.block_id = 0
		self.last_block_end = 0
		self.block_type = BlockType.NONE

	def getBlockType(self):
		return self.block_type