from __future__     import print_function

# Beautiful COLors for formatting
class bcol:
    MAGENTA    = '\033[95m'
    BLUE       = '\033[94m'
    GREEN      = '\033[92m'
    YELLOW     = '\033[93m'
    RED        = '\033[91m'
    BOLD       = '\033[1m'
    UNDERLINE  = '\033[4m'
    END        = '\033[0m'

    @staticmethod
    def H(msg):
        print(bcol.MAGENTA + msg + bcol.END)

    @staticmethod
    def I(msg):
        print(bcol.BLUE + msg + bcol.END)

    @staticmethod
    def W(msg):
        print(bcol.YELLOW + msg + bcol.END)

    @staticmethod
    def OK(msg):
        print(bcol.GREEN + msg + bcol.END)

    @staticmethod
    def E(msg):
        print(bcol.RED + msg + bcol.END)