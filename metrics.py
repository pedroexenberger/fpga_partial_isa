from collections import OrderedDict
parameters_map = {
	#'sim_insts ':'Instructions',
	#'system.cpu.numCycles ':'Cycles',
	'system.cpu.commit.op_class_0::IntMult ':'IntMult',
	'system.cpu.commit.op_class_0::IntDiv ':'IntDiv',
	'system.cpu.commit.op_class_0::IntALU ':'IntAlu',
	'system.cpu.commit.op_class_0::FloatAdd ':'FloatAdd',
	'system.cpu.commit.op_class_0::FloatCmp ':'FloatCmp',
	'system.cpu.commit.op_class_0::FloatCvt ':'FloatCvt',
	'system.cpu.commit.op_class_0::FloatMult ':'FloatMult',
	'system.cpu.commit.op_class_0::FloatMultAcc ':'FloatMultAcc',
	'system.cpu.commit.op_class_0::FloatDiv ':'FloatDiv',
	'system.cpu.commit.op_class_0::FloatMisc ':'FloatMisc',
	'system.cpu.commit.op_class_0::MemRead ':'MemRead',
	'system.cpu.commit.op_class_0::MemWrite ':'MemWrite',
	'system.cpu.commit.op_class_0::FloatMemRead ':"FloatMemRead",
	'system.cpu.commit.op_class_0::FloatMemWrite ':"FloatMemWrite",
	#'system.cpu.ipc ':'IPC',
	#'system.cpu.dcache.overall_miss_rate::total ':'miss_rate'
}

ordered_parameters_map = OrderedDict([
	('system.cpu.commit.op_class_0::IntMult ','IntMult'),
	('system.cpu.commit.op_class_0::IntDiv ','IntDiv'),
	('system.cpu.commit.op_class_0::IntALU ','IntAlu'),
	('system.cpu.commit.op_class_0::FloatAdd ','FloatAdd'),
	('system.cpu.commit.op_class_0::FloatCmp ','FloatCmp'),
	('system.cpu.commit.op_class_0::FloatCvt ','FloatCvt'),
	('system.cpu.commit.op_class_0::FloatMult ','FloatMult'),
	('system.cpu.commit.op_class_0::FloatMultAcc ','FloatMultAcc'),
	('system.cpu.commit.op_class_0::FloatDiv ','FloatDiv'),
	('system.cpu.commit.op_class_0::FloatMisc ','FloatMisc'),
	('system.cpu.commit.op_class_0::MemRead ','MemRead'),
	('system.cpu.commit.op_class_0::MemWrite ','MemWrite'),
	('system.cpu.commit.op_class_0::FloatMemRead ',"FloatMemRead"),
	('system.cpu.commit.op_class_0::FloatMemWrite ',"FloatMemWrite"),
	])
	#'system.cpu.ipc ':'IPC',
	#'system.cpu.dcache.overall_miss_rate::total ':'miss_rate'
